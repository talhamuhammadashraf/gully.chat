const gcs = require('@google-cloud/storage')(),
    myBucket = gcs.bucket("gullychat-3f64a.appspot.com"),
    stream = require('stream'),
    spawn = require('child-process-promise').spawn,
    path = require('path'),
    os = require('os');

exports.init = function(db) {

    return {
        run: run
    };

    function run(event) {
        if(!event.data._delta) {
            return
        }
        const bufferStream = new stream.PassThrough(),
            paths = event.data._path.split('/'),
            fileName = paths[4],
            file = myBucket.file('menu-image-' + fileName + '.jpg'),
            image = event.data._delta.replace(/^data:image\/\w+;base64,/, "");

        bufferStream.end(new Buffer(image, 'base64'));
        bufferStream.pipe(file.createWriteStream({
                metadata: {
                    contentType: 'image/jpeg',
                    metadata: {
                        custom: 'metadata'
                    }
                },
                public: true,
                validation: "md5"
            }))
            .on('error', function(err) {
            })
            .on('finish', function(res) {
                const dbPath = event.data._path.replace('/imageUrl', ""),
                    ref = db.ref(dbPath),
                    THUMB_PREFIX = 'thumb_',
                    filePath = file.metadata.name,
                    fileDir = path.dirname(filePath),
                    thumbFilePath = path.normalize(path.join(fileDir, THUMB_PREFIX + fileName + '.jpg')),
                    tempLocalFile = path.join(os.tmpdir(), filePath),
                    tempLocalThumbFile = path.join(os.tmpdir(), thumbFilePath);
                myBucket.file(file.metadata.name).download({
                    destination: tempLocalFile
                }).then(function() {
                    //Generate a thumbnail using ImageMagick.
                    spawn('convert', [tempLocalFile, '-thumbnail', '200x200>', tempLocalThumbFile]).then(function() {
                        //uploading thumbnail to google cloud
                        myBucket.upload(tempLocalThumbFile, {destination: thumbFilePath, public: true}).then(function(){
                            var thumbUrl = myBucket.file(thumbFilePath);
                            //getting metadata for url
                            thumbUrl.getMetadata((function(err, result){
                                //uploading image
                                ref.update({imageUrl: file.metadata.mediaLink, thumbUrl: result.mediaLink});
                            }));
                        })
                    });
                });
            });

    }

};

