
/**
 * Firebase functions listeners
 */

/**
 * ---------------------------------------------------------------------------------------------------------------------
 * Core library
 * ---------------------------------------------------------------------------------------------------------------------
 */
const functions = require('firebase-functions');
const admin = require('firebase-admin');

/**
 * ---------------------------------------------------------------------------------------------------------------------
 * Variable initialization
 * ---------------------------------------------------------------------------------------------------------------------
 */
//Database initialization
admin.initializeApp(functions.config().firebase);
const db = admin.database();
const SendPicture = require('./SendPicture').init(db);
const ChatPicture = require('./ChatPicture').init(db);
const ChatCounts = require('./ChatCounts').init(db);
///ask-genius/request/{requestId}
//https://gullychat-3f64a.firebaseio.com/business/-KxlP4ipLPmAsrCeA8zm/images/-KxlP4lF22SLxuXrx3AT/imageUrl
exports.pushNotification = functions.database.ref('/businesses/{businessId}/isApproved')
    .onUpdate(event => {
        var isApproved = event.data.val()

        // This registration token comes from the client FCM SDKs.

        var tokenDatabase = event.data.ref.parent.child("userId").once("value", function (snap) {
            var userID = snap.val();
            console.log(userID, "ID")
            db.ref("users").child(userID).child("deviceID").once("value", (snap) => {
                console.log(snap.val())
                var registrationToken = snap.val()
                console.log(registrationToken, "token")
                // event.data.ref.parent.parent.parent.child("users").child(userID).child("deviceID")                
                // See the "Defining the message payload" section below for details
                // on how to define a message payload.
                event.data.ref.parent.child("businessName").once("value", (snap) => {
                    var bizName = snap.val()
                    console.log(bizName, "bizName")
                    if (isApproved) {
                        var payload = {
                            notification: {
                                id:"businessNotification",
                                title: bizName + " Approved",
                                body: 'Congratulation Your new business ' + bizName + " has been approved",
                                sound: 'default'
                            },
                            data: {screen: 'ChatsList'}
                        };
                    }
                    admin.messaging().sendToDevice(registrationToken, payload)
                        .then(function (response) {
                            console.log("done",response)
                            //See the MessagingDevicesResponse reference documentation for
                            //the contents of response.
                        })
                        .catch(function (error) {
                            console.log(error)
                        });
                    })

                })
            });
});


exports.pushMessage=functions.database.ref("/message/{businessId}/{messageId}").onCreate(event=>{
    var chatObj=event.data.val();
    db.ref("users").child(chatObj.businessId).once("value").then((snap)=>{
    if(snap.exists()){
    console.log("ok userToBusinesses false")
    var userObj=snap.val();
    console.log(userObj,"userObj")
    var deviceID=userObj.deviceID;
    console.log(deviceID,"deviceID")
    var message=chatObj.text;
    console.log(message,"message")
    var sender=chatObj.user.name;
    console.log(sender,"sender");
        db.ref("businesses").orderByChild("userId").equalTo(chatObj.businessId).once("value").then((snap)=>{
            var payload={ notification: {
                title:sender,
                body:message,
                sound: 'default'
            },
                data: {
                    chatObj: JSON.stringify(snap.val()),
                    userToBusinesses: 'false'
                }
            }
        });
        console.log('snap....payload....', payload);
admin.messaging().sendToDevice(deviceID,payload).then(()=>console.log("hogya")).catch(()=>console.log("nahi hua"))    
}
    else{
    console.log("not okay userToBusinesses true")
    db.ref("businesses").child(chatObj.businessId).once("value").then((snap)=>{
        var businessObj=snap.val();
        console.log(businessObj,"businessObj")
        var userId=businessObj.userId;
        console.log(userId,"userId");
        db.ref("users").child(userId).once("value").then((snap)=>{
        var userObj=snap.val();
        console.log(userObj,"userObj")
        var deviceID=userObj.deviceID;
        console.log(deviceID,"deviceID")
        var message=chatObj.text;
        console.log(message,"message")
        var sender=chatObj.user.name;
        console.log(sender,"sender")
            var payload={ notification: {
                title: sender,
                body: message.toString(),
                sound: 'default'
            },
                data: {
                    chatObj: JSON.stringify(businessObj),
                    userToBusinesses: 'true'
                }
            };
    console.log("pay", payload)
    admin.messaging().sendToDevice(deviceID, payload).then(()=>console.log("hogya")).catch((err)=>console.log("nahi hua bhai", err))
})
})
}
})
})

exports.processImages = functions.database.ref('/businesses/{businessId}/images/{imageId}/imageUrl')
    .onWrite(SendPicture.run);

exports.chatImages = functions.database.ref('/message/{roomId}/{messageId}/imageUrl')
    .onWrite(ChatPicture.run);

exports.countMessages = functions.database.ref('/message/{roomId}/{messageId}')
    .onWrite(ChatCounts.count);

