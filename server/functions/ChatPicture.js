const gcs = require('@google-cloud/storage')(),
    myBucket = gcs.bucket("gullychat-3f64a.appspot.com"),
    stream = require('stream'),
    spawn = require('child-process-promise').spawn,
    path = require('path'),
    os = require('os');

exports.init = function(db) {

    return {
        run: run
    };

    function run(event) {
        if(!event.data._delta) {
            return
        }
        const bufferStream = new stream.PassThrough(),
            paths = event.data._path.split('/'),
            fileName = paths[2],
            file = myBucket.file('chat-image-' + fileName + '.jpg'),
            image = event.data._delta.replace(/^data:image\/\w+;base64,/, "");

        bufferStream.end(new Buffer(image, 'base64'));
        bufferStream.pipe(file.createWriteStream({
                metadata: {
                    contentType: 'image/jpeg',
                    metadata: {
                        custom: 'metadata'
                    }
                },
                public: true,
                validation: "md5"
            }))
            .on('error', function(err) {
            })
            .on('finish', function(res) {
                const dbPath = event.data._path.replace('/imageUrl', ""),
                    ref = db.ref(dbPath);
                ref.update({image: file.metadata.mediaLink, imageUrl: null});
            });

    }

};

