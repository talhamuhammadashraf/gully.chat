exports.init = function(db) {

    return {
        count: count,
    };

    function count(event) {
        const collectionRef = event.data.ref.parent.parent.parent,
        paths = event.data._path.split('/'),
            roomId = paths[2];
        const countRef = collectionRef.child('metadata').child(roomId).child('messages_count');

        // Return the promise from countRef.transaction() so our function
        // waits for this async event to complete before it exits.
        return countRef.transaction(current => {
            if (event.data.exists() && !event.data.previous.exists()) {
                return (current || 0) + 1;
            }
            else if (!event.data.exists() && event.data.previous.exists()) {
                return current ? current - 1 : 0;
            }
        }).then(() => {
        });
    }
};

