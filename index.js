import React, { Component } from 'react';
import {
    Text,
    View,
    AppRegistry
} from 'react-native';

import App from './App';
import {
    setCustomTextInput,
    setCustomText,
} from 'react-native-global-props';

// Getting rid of that ugly line on Android and adding some custom style to all TextInput components.
const customTextInputProps = {
    underlineColorAndroid: 'rgba(0,0,0,0)',
    style: {
        fontFamily: 'OpenSans-Regular',
    }
};

// Setting default styles for all Text components.
const customTextProps = {
    style: {
        fontFamily: 'OpenSans-Regular',
        color: 'black'
    }
};

setCustomTextInput(customTextInputProps);
setCustomText(customTextProps);

AppRegistry.registerComponent('Talha', () => App);
