import firebase from 'react-native-firebase';


export const UPDATE_USER = (user,role) => {

    return dispatch => {
        if(user && role){
            user['role'] = role;
        }
        dispatch({
            type: 'USER',
            user
        });
    }
};

export const UPDATE_USER_ADDRESS = (user) =>{
    return dispatch => {
        dispatch({
            type: "USER_ADDRESS",
            user
        })

    }
}

export const UPDATE_USER_PRESSENCE = () =>{
    return dispatch => {
        var userPressence;   
        firebase.database().ref("presence").on("value",(snap)=>{
        userPressence = snap.val()
        dispatch({
            type: "UPDATE_USER_PRESSENCE",
            userPressence
        })
    })

    }
}