import firebase from 'react-native-firebase';
import geoFire from 'geofire';
import _ from 'underscore';


export const UPDATE_STORE = (location) => {
    return dispatch => {
        //var myLocation = Object.values(location).reverse();
        var myLocation = Object.values(location);
        var businessRef = firebase.database().ref('businesses');
        businessRef.on('value', (snapshot) => {
            var distanceRef = [];
            var object = snapshot.val();
            for(var key in object){
                object[key]['_id'] = key;
                object[key].isApproved && distanceRef.push(object[key]);
            }
            distanceRef = calcDistance(distanceRef, myLocation);
            var result = _.sortBy(distanceRef, 'distance');
            dispatch({
                type: 'UPDATE_STORE',
                value :  result
            });
        })
    }
};

export const LISTEN_PRESENCE = () => {
    return dispatch => {

        var presenceRef = firebase.database().ref('presence');
        presenceRef.on('child_added', (snapshot) => {
            dispatch({
                type: 'ONLINE',
                uid :  snapshot.key
            });
        });
        presenceRef.on('child_removed', (snapshot) => {
            dispatch({
                type: 'OFFLINE',
                uid :  snapshot.key
            });
        });
    }
};

export const LISTEN_CATEGORIES = () => {
    return dispatch => {
        var presenceRef = firebase.database().ref('categories');
        presenceRef.on('child_added', (snapshot) => {
            dispatch({
                type: 'ADD_CATEGORY',
                value :  snapshot.val(),
                key: snapshot.key
            });
        });
        presenceRef.on('child_changed', (snapshot) => {
            dispatch({
                type: 'CHANGE_CATEGORY',
                value :  snapshot.val(),
                key: snapshot.key
            });
        });
        presenceRef.on('child_removed', (snapshot) => {
            dispatch({
                type: 'REMOVE_CATEGORY',
                key: snapshot.key
            });
        });
    }
};

export const LISTEN_PAYMENTS = () => {
    return dispatch => {
        var presenceRef = firebase.database().ref('payments');
        presenceRef.on('child_added', (snapshot) => {
            let value = snapshot.val();
                dispatch({
                    type: 'ADD_PAYMENTS',
                    value :  snapshot.val(),
                    key: snapshot.key
                });
        });
        presenceRef.on('child_changed', (snapshot) => {
            dispatch({
                type: 'CHANGE_PAYMENTS',
                value :  snapshot.val(),
                key: snapshot.key
            });
        });
        presenceRef.on('child_removed', (snapshot) => {
            dispatch({
                type: 'REMOVE_PAYMENTS',
                key: snapshot.key
            });
        });
    }
};

export const GET_USER_BUSINESS = (uid) => {

    return dispatch => {
        var presenceRef = firebase.database().ref('businesses');
        presenceRef.orderByChild('userId').equalTo(uid).on('value', (snapshot) => {
            let values = snapshot.val();
                if(values){
                    let key = Object.keys(values)[0];

                    dispatch({
                        type: 'USER_BUSINESS',
                        value :  values[key],
                        key: key
                    });
                } else {
                    dispatch({
                        type: 'USER_BUSINESS',
                        value :  {},
                        key: ''
                    });
               }

        });
    }
};

export const TOGGLE_BUSINESS = (bool) => {
    return dispatch => {
        dispatch({
            type: 'BUSINESS_ACTIVATED',
            value :  bool
        });
    }
};

const calcDistance = (calcDistance, myloc) => {
    return calcDistance.filter((items)=> {
        if(items.address){
            var storeLoc = Object.values(items.address.address);
            storeLoc = [parseFloat(storeLoc[0]), parseFloat(storeLoc[1])];
            items.distance = Number(geoFire.distance(myloc, storeLoc).toFixed(1));
        }
        return items.distance < 20;
    });
};
