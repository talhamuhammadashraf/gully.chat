import firebase from 'react-native-firebase';

export const UPDATE_LOCATION = (location) => {
    return dispatch => {
        dispatch({
            type: 'UPDATE_LOCATION',
            location
        });
    }
};