import _ from 'underscore'
import firebase from 'react-native-firebase'
const initialState = {
    categories: [],
    payments: [],
};
/*reducer will update the state in store*/
const storeReducer = (state = initialState, action)=>{
    switch (action.type) {

        case 'UPDATE_STORE':
            var k = action.value ? updateStorePresence(action.value, action.uid, true) : [];
            return {...state, stores: action.value ? updateStorePresence(action.value, action.uid, true) : []};

        case 'ADD_CATEGORY':
            var categories = state.categories;
            action.value.key = action.key;
            !_.findWhere(categories, {key: action.key}) && categories.push(action.value);
            return {...state, categories: approvedItems(categories)};

        case 'CHANGE_CATEGORY':
            var categories = state.categories,
                index = categories.findIndex(x => x.key == action.key);
            action.value.key = action.key;
            index == -1 ? categories.push(action.value) : (categories[index] = action.value)
            return {...state, categories: approvedItems(categories)};

        case 'REMOVE_CATEGORY':
            var categories = state.categories,
                index = categories.findIndex(x => x.key == action.key);
                if(index != -1) {
                    categories.splice(index, 1);
                }
            return {...state, categories: approvedItems(categories)};

        case 'ADD_PAYMENTS':
            var payments = state.payments;
            action.value.key = action.key;
            !_.findWhere(payments, {key: action.key}) && payments.push(action.value);
            return {...state, payments: approvedItems(payments)};

        case 'CHANGE_PAYMENTS':
            var payments = state.payments,
                index = payments.findIndex(x => x.key == action.key);
            action.value.key = action.key;
            index == -1 ? payments.push(action.value) : (payments[index] = action.value)
            return {...state, payments: approvedItems(payments)};

        case 'REMOVE_PAYMENTS':
            var payments = state.payments,
                index = payments.findIndex(x => x.key == action.key);
            if(index != -1) {
                payments.splice(index, 1);
            }
            return {...state, payments: approvedItems(payments)};

        case 'USER_BUSINESS':
            if(action.key){
                action.value._id = action.key;
            }
            return {...state, userBusiness: action.value};

        case 'BUSINESS_ACTIVATED':
            return {...state, businessActivated: action.value,};

        case 'ONLINE':
            return {...state, stores: updateStorePresence(state.stores, action.uid, true)};

        case 'OFFLINE':
            return {...state, stores: updateStorePresence(state.stores, action.uid, false)};

        default:
            return state
    }
};

const updateStorePresence = (stores, uid, bool)=>{
    return stores && stores.map((elem) => {
        return elem;
    })
};

const presenceInUserBusiness = (business)=>{
    business &&
    firebase.database().ref("presence").child(business.userId).on("value",(snap)=>{
        business.isOnline = snap.val() ? true :false
    })
    return business

};


const approvedItems = (items) => {
    return items.filter((item) => !item.notApproved)
};

export default storeReducer;