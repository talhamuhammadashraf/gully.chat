import { combineReducers } from "redux";
import userReducer from './userReducer.js'
import locationReducer from './locationReducer.js'
import storeReducer from './storeReducer.js'
import chatReducer from './chatReducer.js'

//import imagesReducer from "./imagesReducer";
//import billingReducer from "./billingReducer";

const reducers = combineReducers({
  //TODO add more reducers here
  //course,
  //Change these reducers for what values i need them to get me. 
  //For example posts, sign in, signup, login, written posts, courses etc ...
  //error should equal the error message that the server gives
  userReducer: userReducer,
  locationReducer: locationReducer,
  storeReducer : storeReducer,
  chatReducer : chatReducer
});

//export default rootReducer;
export default rootReducer = (state, action) => {

  return reducers(state, action);
}