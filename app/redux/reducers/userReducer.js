const initialState = {

};
/*reducer will update the state in store*/
const userReducer = (state = initialState, action)=>{

    switch (action.type) {
        case 'USER':
            return { ...state, user: action.user };
        case 'USER_ADDRESS':
            return { ...state, user: action.user };
        case 'UPDATE_USER_PRESSENCE':
            return { ...state, userPressence: action.userPressence || {} };
        // case 'ONLINE':
        //     return { ...state, online: updateStorePresence(state.stores, action.uid, true) };

        // case 'OFFLINE':
        //     return { ...state, stores: updateStorePresence(state.stores, action.uid, false) };

        default:
            return state
    }
};

export default userReducer;