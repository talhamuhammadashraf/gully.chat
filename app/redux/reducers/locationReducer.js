const initialState = {

};
/*reducer will update the state in store*/
const locationReducer = (state = initialState, action)=>{

    switch (action.type) {
        case 'UPDATE_LOCATION':
            return {...state, location: action.location};
        default:
            return state
    }
};

export default locationReducer;