
import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Dimensions,
    Platform,
    TextInput,
    Image,
    FlatList,
    ScrollView
} from 'react-native';
import { bindActionCreators } from 'redux';
const { width, height } = Dimensions.get('window');
import { connect } from 'react-redux';
import _ from 'underscore';
import firebase from 'react-native-firebase';

export class Chat extends Component{
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
    }

    render(){
        var {messages, uid} = this.props;
      return(
              <View style={styles.container}>
                  <ScrollView>
                      <View style={{height: 40, borderBottomWidth: 1, justifyContent: 'center', paddingLeft: 15, backgroundColor: 'white', borderColor: '#efefef'}}>
                      </View>
                      <FlatList
                          keyExtractor={(item, index) => index}
                          data={messages}
                          renderItem={({item}) =>
                          <View style={styles.touchBorder} onPress={() => this.gotoChat(item)}>
                                <View style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    alignItems: 'stretch'
                                   }}>
                                   {item.userId != uid ?
                                    <View style={{flex: 1, flexDirection: 'row',  height: 35}}>
                                        <Image source={require('../../../assets/online.png')} style={{width: 30, height: 30}}/>
                                        <Text style={[styles.text, styles.textLeft]}>{item.message} </Text>
                                    </View>
                                    :
                                    <View style={{flex: 1, flexDirection: 'row',  height: 35, justifyContent: 'flex-end'}}>
                                        <Text style={[styles.text, styles.textRight]}>{item.message} </Text>
                                        <Image source={require('../../../assets/online.png')} style={{width: 30, height: 30}}/>
                                    </View>
                                    }
                                 </View>
                          </View>
                    }
                          />
                  </ScrollView>

              </View>
      )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        backgroundColor: 'white'
    },
    businessName: {
        fontWeight: '100',
        padding: 10,
        fontSize: 16,
        height: 55,
        color: '#333333'
    },
    item: {
        fontSize: 12,
        color: '#4F4F4F',
        paddingLeft: 10,
        paddingTop: 2,
        textDecorationLine: 'underline'
    },
    text: {
        color: 'white', fontSize: 15, paddingLeft: 10, borderRadius: 10
    },
    textLeft: {
        backgroundColor: '#4990d0'
    },
    textRight: {
        backgroundColor: 'gray'
    },
    touchBorder: {
        margin: 10
    }
});

const mapStateToProps = (state) => {
    return {
        user: state.userReducer.user
    };
};


export default connect(
    mapStateToProps
)(Chat)