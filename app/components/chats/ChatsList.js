
import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Dimensions,
    Platform,
    TextInput,
    Image,
    FlatList,
    ScrollView
} from 'react-native';
import { bindActionCreators } from 'redux';
const { width, height } = Dimensions.get('window');
import { connect } from 'react-redux';
import SvgUri from 'react-native-svg-uri';
import _ from 'underscore';
import firebase from 'react-native-firebase';
import { TOGGLE_BUSINESS } from '../../redux/actions/storeAction';
import moment from 'moment';
import Avatar from '../avatar/Avatar';

export class ChatsList extends Component{
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            businessRoute: props.navigation.state.routeName != "ChatsTab",
            chats: []
        };
        this.getChats(props.navigation.state.routeName == "ChatsTab" ? {userId : props.user.uid} : {businessId: props.userBusiness._id})

    }

    componentWillMount(){
        this.props.TOGGLE_BUSINESS(this.props.navigation.state.routeName != "ChatsTab")
    }

    componentWillUnmount(){
        this.props.TOGGLE_BUSINESS(false)
    }

    getChats(param) {
        let database = firebase.database(),
            ref = database.ref('metadata');
        ref.orderByChild(Object.keys(param)[0]).equalTo(Object.values(param)[0]).on("value", (snapshot) => {
            this.setState({chats: _.map(snapshot.val(), (chat, index) => {

               let readCount = Object.values(snapshot.val());
                var chat = chat;

                chat._id = index;
                return chat;
            })})
        });
    }

    goToChat(item, query ,online){
        firebase.database().ref(query.key + '/' + query.value).once('value', (snapshot) => {
            let param = snapshot.val();
            param._id = snapshot.key;
            this.props.navigation.navigate('Info', {data: param,online:online, roomId: item._id, userToBusiness: query.key == "businesses"});
        });
    }

    render(){
        var {businessRoute,chats} = this.state;
        var {userBusiness, businessActivated, navigation} = this.props;
        return(
            <View style={styles.container}>

                <ScrollView>

                    {businessRoute && <View style={{height: 40, borderBottomWidth: 1, justifyContent: 'center', backgroundColor: 'white', borderColor: '#efefef'}}>
                        <Text style={{fontSize: 18, fontWeight: 'bold', color: '#333333', textAlign: 'center'}}>Chat Orders</Text>
                    </View>}

                    {!this.state.chats.length && userBusiness && userBusiness.isApproved && <View style={{flex: 1,flexDirection: 'column'}}>
                        <View style={{height:height*0.35,alignItems:'center',justifyContent:'center'}} >
                            <Text style={{fontSize : 30}}>No orders yet!</Text>
                            <Text style={{fontSize : 17,color:'#A9A9A9'}}>Your business profile is live, you will</Text>
                            <Text style={{fontSize : 17,color:'#A9A9A9'}}>see live chat orders here</Text>
                        </View>
                    </View>}

                    {navigation.state.routeName != "ChatsTab" && userBusiness && !userBusiness.isApproved ? <View style={{padding: 15}}>
                            <View style={{alignItems:'center',marginTop:10,justifyContent:'center'}}>
                                <SvgUri width="70" height="70" svgXmlData='<?xml version="1.0" encoding="iso-8859-1"?><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"	 viewBox="0 0 491.5 491.5" style="enable-background:new 0 0 491.5 491.5;" xml:space="preserve"><g>	<g>		<path style="fill:#2C2F33;" d="M65,387.897h33.3v87.8c0,4,2.4,7.5,6,9.1c1.3,0.5,2.6,0.8,3.9,0.8c2.5,0,5-1,6.9-2.8l99.1-94.9H400			c26.4,0,50.7-16.6,60.5-41.3c2-5.1-0.5-10.8-5.6-12.8s-10.8,0.5-12.8,5.6c-6.8,17.2-23.7,28.7-42.1,28.7H210.2c-2.6,0-5,1-6.8,2.8			l-85.2,81.6v-74.4c0-5.5-4.4-9.9-9.9-9.9H65c-24.9,0-45.2-20.3-45.2-45.2v-204.5c0-24.9,20.3-45.2,45.2-45.2h205.4			c5.5,0,9.9-4.4,9.9-9.9s-4.4-9.9-9.9-9.9H65c-35.9,0-65,29.2-65,65v204.4C0,358.797,29.1,387.897,65,387.897z"/>		<path style="fill:#3C92CA;" d="M255.8,129.197c-3.6-4.7-9.2-7.8-15.6-7.8H193c-10.8,0-19.5,8.8-19.5,19.5v144.1			c0,10.8,8.8,19.5,19.5,19.5h47.2c5.1,0,9.7-2,13.2-5.2c8.3,8.2,19.7,13.4,32.3,13.4h133.8c30.8,0,50.4-16.2,53.9-44.3l18-114.3			c0.1-0.5,0.1-1,0.1-1.5c0-25.2-20.5-45.7-45.7-45.7h-65.5v-46.7c0-21.3-6.3-36.7-18.7-45.8c-19.6-14.4-46.4-6.2-47.6-5.9			c-4.1,1.3-6.9,5.1-6.9,9.4v54.7c0,41.5-48.5,55.7-50.5,56.3C256.2,128.997,256,129.097,255.8,129.197z M239.9,284.797h-46.7			v-143.6h46.7V284.797z M259.7,148.297c0.7,0,1.4-0.1,2.1-0.3c2.7-0.7,65-18.8,65-75.4v-46.6c6.4-0.8,16-0.8,23,4.4			s10.6,15.2,10.6,29.8v56.5c0,5.5,4.4,9.9,9.9,9.9h75.4c14.1,0,25.5,11.2,25.9,25.2l-17.9,113.7c0,0.1,0,0.2,0,0.3			c-2.2,18-13.7,27.1-34.3,27.1H285.7c-14.3,0-25.9-11.6-25.9-25.9v-118.7L259.7,148.297L259.7,148.297z"/>	</g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>' /></View>
                        <Text style={{marginTop: 20, color: '#749d69', fontSize: 30, textAlign: 'center'}}>Awesome,
                            Profile submitted for approval</Text>
                        <Text style={{marginTop: 20, textAlign: 'center',color:'#8a8f96',fontSize:18}}>You will receive an approval call within two
                            business days before your business profile go live. </Text>
                    </View>
                    :
                    <FlatList
                        keyExtractor={(item, index) => index}
                        data={chats}
                        renderItem={({item}) =>
                            <TouchableOpacity style={styles.touchBorder} onPress={() => this.goToChat(item, businessActivated ? {key: 'users', value: item.userId} : {key: 'businesses', value: item.businessId}
                            ,this.props.userPressence[businessActivated ? item.userId :item.ownerId] || false
                        )}>
                                <View style={{
                                        flex: 1,
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        alignItems: 'stretch'
                                       }}>
                                        <View style={{flex: 1, flexDirection: 'row',paddingLeft:3}}>
                                            <Avatar
                                                name={businessRoute ? item.userName && item.userName.toUpperCase() : item.businessName && item.businessName.toUpperCase()}
                                                containerStyle={{alignSelf: 'center', marginLeft: 3}}/>
                                            {this.props.userPressence[businessActivated ? item.userId :item.ownerId]  ? <Image source={require('../../../assets/online.png')} style={styles.status}/> : <Image source={require('../../../assets/offline.png')} style={styles.status}/> }
                                        </View>
                                        <View style={{flex: 4, flexDirection: 'column', justifyContent: 'center'}}>
                                            <View>
                                                <Text style={styles.businessName} numberOfLines={1}>{businessRoute ? item.userName && item.userName.toUpperCase() : item.businessName && item.businessName.toUpperCase()}</Text>
                                            </View>
                                            <View>
                                                <Text style={{color: '#a2a2a2', fontSize: 15, marginTop: 10}} numberOfLines={1}>{item.lastMessageText}</Text>
                                            </View>
                                        </View>


                                    <View style={{flex: 1,flexDirection: 'column'}}>

                                        {!!item.unread && !!this.props.user._user && !!item.unread[this.props.user._user.uid] ? <View style={{ alignItems: 'center', justifyContent: 'center'}}>{console.log(item,"@@@@")}
                                                <View style={{ marginTop:5,backgroundColor: 'rgb(93,163,227)', height: 20, width: 20, borderRadius: 100, marginRight: 10, alignItems: 'center', justifyContent: 'center' }}>
                                                    <Text style={{ color: '#FFFFFF', fontWeight: 'bold' }}>
                                                        {(item.unread[this.props.user._user.uid])}
                                                    </Text>
                                                </View>
                                            </View> :
                                            <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
                                                <View>
                                                    <Text style={{ color: '#cecece', fontSize: 15, marginTop: 15 }} numberOfLines={1}>{moment(item.updatedAt).fromNow(true)}</Text>
                                                </View>

                                            </View>
                                        }

                                        <View>
                                            <Text style={{ fontSize: 30, color: '#cecece',marginLeft:7}} numberOfLines={1}> > </Text>
                                        </View>
                                    </View>

                                </View>
                            </TouchableOpacity>
                    }
                    />}
                </ScrollView>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        backgroundColor: 'white'
    },
    businessName: {
        fontWeight: '500',
        fontSize: 16,
        color: '#333333',
        marginTop: 6
    },
    item: {
        fontSize: 12,
        color: '#4F4F4F',
        paddingLeft: 10,
        paddingTop: 2,
        textDecorationLine: 'underline'
    },
    text: {
        color: 'white', fontSize: 15, paddingLeft: 10, borderRadius: 10
    },
    status: {
        width: 10,
        height: 10,
        marginLeft: -12,
        marginTop: 35,
        alignSelf: 'center',
        borderColor: 'white',
        borderWidth: 2,
        borderRadius: 10
    },
    textLeft: {
        backgroundColor: '#4990d0'
    },
    textRight: {
        backgroundColor: 'gray'
    },
    touchBorder: {
        borderBottomWidth: 1,
        paddingBottom: 7,
        borderColor: '#eaeaea'
    }
});

const mapStateToProps = (state) => {
    return {
        user: state.userReducer.user,
        userBusiness: state.storeReducer.userBusiness,
        businessActivated: state.storeReducer.businessActivated,
        userPressence: state.userReducer.userPressence
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        TOGGLE_BUSINESS,

    }, dispatch)
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ChatsList)