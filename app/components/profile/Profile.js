
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    Image,
    TextInput,
    View,
    Button,
    Picker,
    TouchableOpacity,
    Dimensions,
    ScrollView,
    Alert,
    Keyboard,
    Modal
} from 'react-native';

import ImagePicker from 'react-native-image-crop-picker';

import ChatsTab from '../chatsTab/ChatsTab'
import ModalDropdown from 'react-native-modal-dropdown';
import GullyCamera from '../camera/Camera'
import SvgUri from 'react-native-svg-uri';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import AutoTags from '../autoTag/AutoTag';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';

import firebase from 'react-native-firebase';
import _ from 'underscore';

const {height, width} = Dimensions.get('window');

const GooglePlacesInput = (props) => {
    return (
        <GooglePlacesAutocomplete
            placeholder={props.businessAddress}
            minLength={2} // minimum length of text to search
            autoFocus={false}
            value = {props.value}
            returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
            textInputProps={{onFocus: () => props.onFocus()}}
            fetchDetails={true}
            renderDescription={row => row.description} // custom description render
            onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                    props.update(data.description);
            }}

            //getDefaultValue={() => ''}

            query={{
        // available options: https://developers.google.com/places/web-service/autocomplete
        key: 'AIzaSyA_B8Bx2QlzoP31krA92nWYvbwbg29MVks',
        language: 'en', // language of the results
        types: 'geocode' // default: 'geocode'
      }}

        styles={{
        textInputContainer: {
            width: '100%',
            backgroundColor: '#fafafa',
            borderTopWidth: 0,
            borderBottomWidth: 0,
            borderLeftWidth: 0,
            borderRightWidth: 0,
            height: 70
        },
        row:{
          backgroundColor: '#fafafa'
        },
        textInput: {
            backgroundColor: '#fafafa',
            fontSize: 20,
            height: 50,
            marginLeft: 5
        },
        description: {
          backgroundColor: '#fafafa',
          fontSize: 20,
          paddingLeft: 3,
          paddingTop: 4,
          paddingBottom: 4,
          height: 35
        },
        container: {
            flex: 0,
            borderTopColor: '#efefef',
            borderBottomColor: '#efefef',
            borderTopWidth: 1,
            borderBottomWidth: 1
        }
      }}
            nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
            GoogleReverseGeocodingQuery={{
        // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
      }}
            GooglePlacesSearchQuery={{
        // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
        rankby: 'distance',
        types: 'food'
      }}

            filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities

            debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
            />
    );
};


export class Profile extends Component{
    constructor(props){
        super(props);
        let business = this.props.userBusiness;
        this.state = {
            page: 0,
            suggestionsCategories : props.categories,
            suggestionsPayments :  props.payments,
            images: _.pluck(business.images,'thumbUrl'),
            businessName : business.businessName || '',
            categoriesSelected : business.categoriesSelected || [],
            businessAddress : business.address && business.address.businessAddress || [],
            address : business.address && business.address.address || '',
            day1 : business.day ? business.day.day1 || '' : 'Mon',
            day2 : business.day ? business.day.day2 || '' : 'Fri',
            time1 : business.time ? business.time.time1 || '' : '12am',
            time2 : business.time ? business.time.time2 || '' : '9pm',
            paymentsSelected : business.paymentsSelected || [],
            cameraVisible: false,
            key: null,
            loading: {}
        };
       let img = this.state.images;
       let imgCount = img.concat(["","","","",""]);
           imgCount.length = 5;
          this.state.images = imgCount;
         this.handleAddition.bind(this)

    }

    componentDidUpdate(props){
        if(this.props.categories.length != props.categories.length){
            this.setState({suggestionsCategories: this.props.categories})
        }
        if(this.props.payments.length != props.payments.length){
            this.setState({suggestionsPayments: this.props.payments})
        }
    }

    componentWillMount () {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
    }

    componentWillUnmount () {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow () {
        this.setState({ keyboardOpened: true })
    };

    _keyboardDidHide () {
        this.setState({ keyboardOpened: false })
    };

    _openCamera(){
        this.setState({cameraVisible: true});
    }

    _closeCamera(){
        this.setState({cameraVisible: false});
    }

    handleDelete = index => {
        if(this.state.page == 0){
            let categoriesSelected = this.state.categoriesSelected;
            categoriesSelected.splice(index, 1);
            this.setState({ categoriesSelected });
        }else{
            let paymentsSelected = this.state.paymentsSelected;
            paymentsSelected.splice(index, 1);
            this.setState({ paymentsSelected });
        }
    };

    handleAddition = suggestion => {
        let {categoriesSelected,paymentsSelected} = this.state;
        (this.state.page == 0) ? categoriesSelected.splice(categoriesSelected.length - 1, 1) : paymentsSelected.splice(paymentsSelected.length - 1 , 1);
        (this.state.page == 0) ? this.setState({ categoriesSelected: categoriesSelected.concat([suggestion]) })
            : this.setState({ paymentsSelected: paymentsSelected.concat([suggestion]) });
    };

    updateAutoTag = (text) => {

        let categoriesSelected = text.split(',');
        !categoriesSelected[0] && (categoriesSelected.length = 0);
        (this.state.page == 0) ? this.setState({categoriesSelected}) : this.setState({paymentsSelected:categoriesSelected})
    };

    addTag = name => {
        firebase.database().ref(this.state.page == 0 ? 'categories' : 'payments').push({
            name: name,
            notApproved: true
        })
    };

    getImageFromCamera(image) {
        this._closeCamera();
        let oldImages = this.state.images;
        oldImages[this.state.key] = image;
        this.setState({images: oldImages});
    }

    getImage(key){
        ImagePicker.openPicker({
            includeBase64: true,
            compressImageQuality : 0.5
        }).then(image => {console.log(image,"imageeeeeeeeee")
            let oldImages = this.state.images;
            oldImages[key] = image;
            this.setState({images: oldImages});
        });
    }

    submitProfile(){
        var database = firebase.database();
        this.setState({page: 2});
        var {businessName, categoriesSelected, day1, day2, time1, time2, paymentsSelected, businessAddress, address} = this.state;
        if(!this.props.userBusiness._id) {
            var key = database.ref('businesses').push({
                businessName: businessName,
                categoriesSelected: categoriesSelected,
                day: {day1: day1, day2: day2},
                time: {time1: time1, time2: time2},
                paymentsSelected: paymentsSelected,
                address: {businessAddress, address},
                userId: this.props.user.uid,
                isApproved: false
            }).key;
        }
        else{
            database.ref('businesses/' + this.props.userBusiness._id ).update({
                businessName: businessName,
                categoriesSelected: categoriesSelected,
                day: {day1: day1, day2: day2},
                time: {time1: time1, time2: time2},
                paymentsSelected: paymentsSelected,
                address: {businessAddress, address},
                userId: this.props.user.uid,
                isApproved: false
            })
            var key = this.props.userBusiness._id
        }

        this.state.images.map(image => {
               database.ref('businesses/' + key).child('images').push({
                   'imageUrl': image.data
               })
        });

    }

    profilePageOne(){
        const {businessName, categoriesSelected, suggestionsCategories, address, businessAddress, page, keyboardOpened} = this.state;
        const {longitude, latitude} = this.props.location;
        let disable = !(businessName && categoriesSelected.length && address && businessAddress);
        return(
            <View style={{flex: 1, flexDirection: 'column', justifyContent: 'space-between', backgroundColor: 'white'}}>
                <ScrollView ref="scrollView">
                <Text style={{paddingLeft: 15, marginTop: 35, color: '#ababab'}}>BUSINESS NAME</Text>
                <TextInput
                    style={styles.input}
                    onChangeText={(businessName) => this.setState({businessName})}
                    underlineColorAndroid="transparent"
                    value={businessName}
                />


                <Text style={{paddingLeft: 15, marginTop: 35, color: '#ababab'}}>BUSINESS CATEGORY/TAGS</Text>

                {(page == 0) && <AutoTags
                    suggestions={suggestionsCategories}
                    tagsSelected={categoriesSelected}
                    handleAddition={this.handleAddition}
                    updateAutoTag={this.updateAutoTag}
                    handleDelete={this.handleDelete}
                    addTag={this.addTag}
                    tagStyles={styles.input}/>}

                <Text style={{paddingLeft: 10, marginTop: 10, color: '#ababab'}}>select or add categories/tags to define your business</Text>

                <Text style={{paddingLeft: 10, marginTop: 35, color: '#ababab'}}>BUSINESS ADDRESS</Text>

                <TextInput
                    style={styles.input}
                    placeholder="Enter complete business address"
                    placeholderTextColor="#A8A8A8"
                    onChangeText={(businessAddress) => this.setState({businessAddress})}
                    value={businessAddress}
                    underlineColorAndroid="transparent"
                />

                <GooglePlacesInput
                    businessAddress = {this.state.address.description || "Enter your city, state, country"}
                    update={(description) => this.setState({address: {description: description, lat: latitude, lng: longitude}})}
                    onFocus={(description) => {setTimeout(() => {
                        this.refs.scrollView.scrollToEnd()
                    }, 1000)}}
                />
                <View style={{height: 60}}>

                </View>

            </ScrollView>
                {keyboardOpened || <View style={{alignItems: 'center', justifyContent: 'center', backgroundColor: disable ? "gray" : "#4990d0", height: 50, width: width}}>
                    <TouchableOpacity disabled={disable} style = {{ width : 400}} onPress={() => this.setState({page: 1})}>
                        <Text style={styles.footerText}>Next ></Text>
                    </TouchableOpacity>
                </View>}
            </View>
        )
    }

    profilePageTwo(){

        const {day1, day2, time1, time2, paymentsSelected, images, suggestionsPayments, page, keyboardOpened, loading} = this.state;
        let disable = !(paymentsSelected.length && _.compact(images).length);
        let business = this.props.userBusiness;

        return(
            <View style={{flex: 1, flexDirection: 'column', justifyContent: 'space-between', backgroundColor: 'white'}}>
            <ScrollView ref="scrollView">
                <Text style={{paddingLeft: 15, marginTop: 35, color: '#ababab'}}>BUSINESS OPERATION TIMING</Text>
                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginBottom: 25, borderWidth: 1, borderColor: '#efefef', height: 70 }}>
                    <View style={{flex: 2, flexDirection: 'row', height: 50, borderWidth: 1, backgroundColor: '#fafafa', borderColor: '#efefef'}}>
                        <View style={{flex: 1}}>
                            <ModalDropdown
                                dropdownTextStyle = {{fontSize : 15, textAlign : 'center'}}
                                dropdownStyle = {{width : 70}}
                                textStyle = {{fontSize : 20}}
                                style = {{flex : 1,alignItems : 'center', justifyContent : 'center'}}
                                onSelect ={(itemIndex,itemValue) => {
                                this.setState({day1: itemValue})
                                }}
                                defaultValue = {business.day ? day1.charAt(0).toUpperCase() + day1.slice(1) : day1}
                                options={['Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat', 'Sun']}>
                            </ModalDropdown>
                        </View>

                        <View style={{flex: 1}}>

                            <ModalDropdown
                                dropdownTextStyle = {{fontSize : 15, textAlign : 'center'}}
                                dropdownStyle = {{width : 70}}
                                textStyle = {{fontSize : 20}}
                                style = {{flex : 1,alignItems : 'center', justifyContent : 'center'}}
                                onSelect = {( itemIndex,itemValue) =>{

                                this.setState({day2: itemValue})}}
                                defaultValue = {business.day ? day2.charAt(0).toUpperCase() + day2.slice(1) : day2}
                                options={['Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat', 'Sun']}>
                            </ModalDropdown>
                        </View>
                    </View>

                    <View style={{flex: 2, flexDirection: 'row', height: 50, borderWidth: 1, backgroundColor: '#fafafa', borderColor: '#efefef'}}>
                        <View style={{flex: 1}}>
                            <ModalDropdown
                                dropdownTextStyle = {{fontSize : 15, textAlign : 'center'}}
                                dropdownStyle = {{width : 70}}
                                textStyle = {{fontSize : 20}}
                                style = {{flex : 1,alignItems : 'center', justifyContent : 'center'}}
                                onSelect = {( itemIndex,itemValue) => {
                                this.setState({time1: itemValue})}}
                                defaultValue = {this.state.time1}
                                options={['12am','1am','2am','3am','4am','5am','6am','7am','8am','9am','10am','11am','12pm',
                                           '1pm','2pm','3pm','4pm','5pm','6pm','7pm','8pm','9pm','10pm','11pm']}>
                            </ModalDropdown>
                        </View>
                        <View style={{flex: 1}}>
                            <ModalDropdown
                                dropdownTextStyle = {{fontSize : 15, textAlign : 'center'}}
                                dropdownStyle = {{width : 70}}
                                textStyle = {{fontSize : 20}}
                                style ={{flex : 1,alignItems : 'center', justifyContent : 'center'}}
                                onSelect = {(itemIndex,itemValue) =>{
                                 this.setState({time2: itemValue})}}
                                defaultValue = {this.state.time2}
                                options={['1am','2am','3am','4am','5am','6am','7am','8am','9am','10am','11am','12am',
                                '1pm','2pm','3pm','pm','5pm','6pm','7pm','8pm','9pm','10pm','11pm','12am'
                                ]}>
                            </ModalDropdown>

                        </View>
                    </View>
                </View>

                <View>
                <Text style={{paddingLeft: 15, marginTop: 35, color: '#ababab'}}>PAYMENT METHODS ACCEPTED</Text>
                </View>
                {(page == 1) && <AutoTags
                    suggestions={suggestionsPayments}
                    tagsSelected={paymentsSelected}
                    handleAddition={this.handleAddition}
                    handleDelete={this.handleDelete}
                    updateAutoTag={this.updateAutoTag}
                    addTag={this.addTag}
                    placeholder="Payment Method" />}
                <Text style={{paddingLeft: 10, marginTop: 10, color: '#ababab'}}>select acceptable methods of payment</Text>

                <Text style={{paddingLeft: 10, marginTop: 35, marginBottom: 10, color: '#ababab'}}>MENU IMAGES</Text>

                    <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-around', marginTop: 10, marginBottom: 10, paddingTop: 10, paddingBottom: 10, borderTopColor: '#eaeaea', borderTopWidth: 1, borderBottomColor: '#eaeaea', borderBottomWidth: 1}}>
                        {
                            images.map((image, key)=>{
                                let img;
                                if(image && image.data) {
                                    img = {opt: {uri: `data:${image.data};base64,`+ image.data, width: image.width, height: image.height}}
                                }
                                else if(image){
                                    img = {opt: {uri: image}}
                                }

                                else {
                                    img = {opt: require('../../../assets/images.png')}
                                }
                                return(<View key={key}>
                                    <TouchableOpacity onPress={() => {
                                        this.setState({key});
                                        this.refs.scrollView.scrollToEnd()
                                        setTimeout(() => {
                                            this['modal' + key].show()
                                        }, 500)
                                    }}>
                                    <Image key={key.toString()}
                                           style={{width: 70, height: 70, borderRadius: 8}}
                                           source= {loading[key] == false ? img.opt : !image ? img.opt : {uri: 'https://cdn.dribbble.com/users/91700/screenshots/3038974/loader.gif'}  }
                                           onLoadStart={(e) => {
                                                let {loading} = this.state;
                                                if(loading[key] != false){
                                                    loading[key] = true;
                                                    this.setState({loading});
                                                }
                                           }}
                                           onLoadEnd={(e) => {
                                                let {loading} = this.state;
                                                loading[key] = false;
                                                this.setState({loading});
                                           }}
                                    /></TouchableOpacity>
                                    <ModalDropdown
                                        ref={el => this['modal' + key] = el}
                                        dropdownTextStyle = {{fontSize : 15, textAlign : 'center', color: 'black'}}
                                        dropdownTextHighlightStyle = {{fontSize : 15, textAlign : 'center', color: 'black'}}
                                        dropdownStyle = {{height: 82}}
                                        showsVerticalScrollIndicator = {false}
                                        textStyle = {{fontSize : 20}}
                                        style = {{flex : 1, alignItems : 'center', justifyContent : 'center'}}
                                        onSelect = {(itemIndex, itemValue) => parseInt(itemIndex) ? this.getImage(key) : this._openCamera()}
                                        options={["Upload Image from Camera", "Upload Image from Gallery"]}>
                                        <Text></Text>
                                        </ModalDropdown>
                                </View>)
                            })
                        }
                    </View>

                <Text style={{paddingLeft: 10, marginTop: 10, color: '#ababab'}}>Upload upto 5 clear images of your latest updated menu or service template:</Text>
                <Text style={{paddingLeft: 10, marginTop: 10, color: '#ababab'}}>Prices per unit should be clearly defined;</Text>
                <Text style={{paddingLeft: 10, marginTop: 10, color: '#ababab'}}>Maximum upload size 5 mb per image</Text>

            </ScrollView>
                {keyboardOpened || <View style={{alignItems: 'center', justifyContent: 'center', backgroundColor: disable ? "gray" : "#4990d0", height: 50, width: width}}>
                    <TouchableOpacity disabled={disable} style={{width : 400 }} onPress={this.submitProfile.bind(this)}>
                        <Text style={styles.footerText}>Next ></Text>
                    </TouchableOpacity>
                </View>}
                <Modal
                    visible={this.state.cameraVisible}
                    animationType={'slide'}
                    onRequestClose={() => this._closeCamera()}>
                    <GullyCamera close={() => this._closeCamera()} getImageFromCamera={(image) => this.getImageFromCamera(image)}/>
                </Modal>
            </View>
        )
    }

    profilePageThree(){
        return(
            <View style={{flex: 1, flexDirection: 'column', justifyContent: 'space-between', backgroundColor: 'white'}}>
                <View style={{height:height*0.01}}></View>
                <View style={{padding: 15}}>
                    <View style={{alignItems:'center',height:height*0.15,justifyContent:'center'}}>
                        <SvgUri width="70" height="70" svgXmlData='<?xml version="1.0" encoding="iso-8859-1"?><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"	 viewBox="0 0 491.5 491.5" style="enable-background:new 0 0 491.5 491.5;" xml:space="preserve"><g>	<g>		<path style="fill:#2C2F33;" d="M65,387.897h33.3v87.8c0,4,2.4,7.5,6,9.1c1.3,0.5,2.6,0.8,3.9,0.8c2.5,0,5-1,6.9-2.8l99.1-94.9H400			c26.4,0,50.7-16.6,60.5-41.3c2-5.1-0.5-10.8-5.6-12.8s-10.8,0.5-12.8,5.6c-6.8,17.2-23.7,28.7-42.1,28.7H210.2c-2.6,0-5,1-6.8,2.8			l-85.2,81.6v-74.4c0-5.5-4.4-9.9-9.9-9.9H65c-24.9,0-45.2-20.3-45.2-45.2v-204.5c0-24.9,20.3-45.2,45.2-45.2h205.4			c5.5,0,9.9-4.4,9.9-9.9s-4.4-9.9-9.9-9.9H65c-35.9,0-65,29.2-65,65v204.4C0,358.797,29.1,387.897,65,387.897z"/>		<path style="fill:#3C92CA;" d="M255.8,129.197c-3.6-4.7-9.2-7.8-15.6-7.8H193c-10.8,0-19.5,8.8-19.5,19.5v144.1			c0,10.8,8.8,19.5,19.5,19.5h47.2c5.1,0,9.7-2,13.2-5.2c8.3,8.2,19.7,13.4,32.3,13.4h133.8c30.8,0,50.4-16.2,53.9-44.3l18-114.3			c0.1-0.5,0.1-1,0.1-1.5c0-25.2-20.5-45.7-45.7-45.7h-65.5v-46.7c0-21.3-6.3-36.7-18.7-45.8c-19.6-14.4-46.4-6.2-47.6-5.9			c-4.1,1.3-6.9,5.1-6.9,9.4v54.7c0,41.5-48.5,55.7-50.5,56.3C256.2,128.997,256,129.097,255.8,129.197z M239.9,284.797h-46.7			v-143.6h46.7V284.797z M259.7,148.297c0.7,0,1.4-0.1,2.1-0.3c2.7-0.7,65-18.8,65-75.4v-46.6c6.4-0.8,16-0.8,23,4.4			s10.6,15.2,10.6,29.8v56.5c0,5.5,4.4,9.9,9.9,9.9h75.4c14.1,0,25.5,11.2,25.9,25.2l-17.9,113.7c0,0.1,0,0.2,0,0.3			c-2.2,18-13.7,27.1-34.3,27.1H285.7c-14.3,0-25.9-11.6-25.9-25.9v-118.7L259.7,148.297L259.7,148.297z"/>	</g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>'/>
                    </View>
                    <Text style={{marginTop: 20, color: '#749d69', fontSize: 30, textAlign: 'center'}}>Awesome, Profile submitted for approval</Text>
                    <Text style={{marginTop: 20, textAlign: 'center',color:'#8a8f96',fontSize:18}}>You will receive an approval call within two business days before your business profile go live.  </Text>
                </View>

                <View style={{alignItems: 'center', justifyContent: 'center', backgroundColor: "#4990d0", height: 50, width: width}}>
                    <TouchableOpacity style = {{width : 400}} onPress={() => this.props.navigation.navigate('ChatsList')}>
                        <Text style={styles.footerText}>Next ></Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    render(){
        var {page} = this.state;
        return (<View style={{flex: 1, backgroundColor: 'white'}}>{
                page == 0 ? this.profilePageOne() : page == 1 ? this.profilePageTwo() : this.profilePageThree()
        }</View>
    )
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    input: {
        height: 60,
        marginTop: 8,
        paddingLeft: 15,
        width: width,
        fontSize: 20,
        borderWidth: 1,
        backgroundColor: '#fafafa',
        borderColor: '#efefef'
    },
    footer: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "#4990d0",
        height: 50,
        width: width
    },
    footerText: {
        color: "white",
        fontSize: 25,
        fontWeight: 'bold',
        textAlign : 'center'
    },
    autoTags: {
        backgroundColor: 'white',
        fontSize: 20
    }
});

const mapStateToProps = (state) => {
    return {
        user: state.userReducer.user,
        location: state.locationReducer.location,
        categories: state.storeReducer.categories,
        payments: state.storeReducer.payments,
        userBusiness : state.storeReducer.userBusiness
    };
};

export default connect(
    mapStateToProps
)(Profile)