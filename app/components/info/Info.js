
import React, {Component} from 'react';
import {View, Text, TextInput, Image , Dimensions, TouchableOpacity, StyleSheet, Keyboard, Modal, Button, Animated} from 'react-native';
import firebase from 'react-native-firebase';
import { bindActionCreators } from 'redux'
import { GiftedChat, Send, Actions } from 'react-native-gifted-chat';
import Login from '../auth/Login';
import Chat from '../chats/Chat';
import GullyCamera from '../camera/Camera'
import {SEND_MESSAGE} from '../../redux/actions/chatAction';
import { connect } from 'react-redux';
import ImageZoom from 'react-native-image-pan-zoom';
import ImagePicker from 'react-native-image-crop-picker';
import _ from 'underscore';

const {height, width} = Dimensions.get('window');
var typingTimer = setTimeout(() => {});
export class Info extends Component{
    static navigationOptions = {
        header: null
    };

    constructor(props){
        super(props);
        this.state = {
            keyboardOpened: false,
            text : '',
            onChat: {},
            unread: {},
            messages : [],
            modalVisible: false,
            cameraVisible: false,
            count: 0,
            limit: 10,
            typingText: false,
            showAddresses: false,
            slideAnim: new Animated.Value(props.navigation.state.params.userToBusiness ? 60 : 86)
        };

        if(props.user){
            this.getChatId()
        }

    }

    componentWillMount () {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
    }

    componentWillUnmount () {
        const {roomId} = this.state;
        roomId &&  this.onChatEnter(roomId, false);
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow () {
        this.setState({ keyboardOpened: true })
    };

    _keyboardDidHide () {
        this.setState({ keyboardOpened: false })
    };

    _openModal(image) {
        this.setState({modalVisible:true, preview: image});
    }

    _closeModal() {
        this.setState({modalVisible:false, preview: null});
    }

    _openCamera(){
        this.setState({cameraVisible: true});
    }

    _closeCamera(){
        this.setState({cameraVisible: false});
    }

    onChatEnter(roomId, mount){
        let user = this.props.user;
        let userId = user.uid;
        let database = firebase.database(),
            metaData =  database.ref('metadata');
        console.log('user', user);
        console.log('userId***', userId);
        console.log('mount***', mount);
        metaData.child(roomId).child('onChat').update({[userId]: mount});
    }

    getChatId(){
        const {data, roomId} = this.props.navigation.state.params;
        let userKey = this.props.user.uid;
        let businessId = data._id;
        if(roomId){
            this.state.roomId = roomId;
            this.initChat(roomId);
        } else {
            let database = firebase.database(),
            metaData =  database.ref('metadata');
            metaData.orderByChild("userId").equalTo(userKey).once("value", (snapshot) => {
                let messKeys = snapshot.val();
                let matchId = _.findWhere(messKeys,{businessId : businessId});
                let _id = matchId ? matchId._id : '';
                this.setState({'roomId' : _id});
                if(_id) {
                    this.initChat(_id);
                }
            });
        }

    };

    initChat(id){
        this.onChatEnter(id, true);
        this.listenMetaData(id);
        this.getInitialMessages(id);
        this.listenMessagesLength(id);
        this.listenTypingStatus(id);
        this.readMessages(id);
    }


    listenMetaData(roomId){
        let refMeta = firebase.database();
        let ref = refMeta.ref('metadata/' + roomId)
        ref.on('value', (snapshot)=>{
            this.setState({'onChat': snapshot.val().onChat || {}, 'unread': snapshot.val().unread || {}})
        })
    }

    readMessages(roomId){
        let { userToBusiness } = this.props.navigation.state.params;
        let user = this.props.user;
        let userId = user.uid;
        let { userBusiness } = this.props;
        let ref = firebase.database().ref('metadata/' + roomId);
        ref.child('unread').update({[userId]: 0});
    }

    listenMessagesLength(roomId){
        let database = firebase.database();

        var ref = database.ref('metadata/' + roomId + '/messages_count');
        ref.on('value', (snapshot) => {
            this.setState({count: snapshot.val()})
        });
    }


    getInitialMessages(roomId){
        let database = firebase.database();
        var ref = database.ref('message/' + roomId).orderByChild("createdAt").limitToLast(this.state.limit).once('value', (snapshot) => {
            let k = _.sortBy(_.map(snapshot.val(), (message) => message), 'createdAt').reverse();
            this.setState((previousState) => ({
                messages: GiftedChat.append(previousState.messages, k),
            }));
            this.listenMessages(roomId)
        });
    }

    getEarlierMessages(roomId, limit){
        let database = firebase.database();
        database.ref('message/' + roomId).orderByChild("createdAt").limitToLast(limit).once('value', (snapshot) => {
            let k = _.sortBy(_.filter(snapshot.val(), (message) => {
                if(!_.findWhere(this.state.messages, {_id: message._id})){
                    return message
                }
            }), 'createdAt').reverse();
            this.setState((previousState) => ({
                messages: GiftedChat.prepend(previousState.messages, k),
                limit
            }));
        });
    }

    listenMessages(roomId){
        let database = firebase.database();
        var ref = database.ref('message').child(roomId);
        ref.limitToLast(1).on('child_added', (snapshot) => {
            if(_.findWhere(this.state.messages, {_id: snapshot.val()._id})){
                return
            }
            let message = snapshot.val();
            message._id = snapshot.key;
            this.setState((previousState) => ({
                messages: GiftedChat.append(previousState.messages, [message]),
            }));
        });
        ref.on('child_changed', (snapshot) => {
            let changedMessage = snapshot.val();
            let messages = this.state.messages.map((message) => {
                return message._id == changedMessage._id ? changedMessage : message
            });
            this.setState((previousState) => ({
                messages: GiftedChat.append(messages, []),
            }));
        });
    }

    listenTypingStatus(roomId){
        let { userToBusiness, data } = this.props.navigation.state.params;
        let database = firebase.database(),
            ref = database.ref('metadata/' + roomId + '/isTyping/' + (userToBusiness ? this.props.navigation.state.params.data.userId : this.props.navigation.state.params.data._id));
        ref.on('value', (snapshot) => {
            this.setState({typingText: snapshot.val()})
        });
    }

    onSend(messages = []) {
        this.setState((previousState) => ({
            messages: GiftedChat.append(previousState.messages, messages),
        }));
    }


    messageSubmit(message, image) {
            let { userToBusiness } = this.props.navigation.state.params;
            let { userBusiness } = this.props;
            let user = this.props.user;
            let userId = user.uid;
            let business = this.props.navigation.state.params.data;
            let sendMessage = message[0] ||{
                    imageUrl: image,
                    createdAt: new Date(),
                    image: 'https://cdn.dribbble.com/users/91700/screenshots/3038974/loader.gif',
                    user: {_id: userToBusiness ? userId : userBusiness._id, name: userToBusiness ? user.displayName : userBusiness.businessName},
            };
            let key = this.state.roomId;
            if(!this.state.roomId) {
                let metaData = {userId: userId, businessId: business._id, createdAt: Date.now(), userName: user.displayName, businessName: business.businessName,ownerId:business.userIds};
                let metadataRef = firebase.database().ref('metadata');
                key = metadataRef.push(metaData).key;
                metadataRef.child(key).update({'_id': key});
                this.setState({roomId : key});
                this.initChat(key);
            }

            sendMessage.businessId = business._id;
            sendMessage.roomId = key;
            sendMessage.read = {[userToBusiness ? userId : userBusiness._id]: true};
            sendMessage.ownerId = userToBusiness ? business.userId : userId;
            if(!this.state.onChat[business.userId] && key){
                let unread = this.state.unread[business.userId] && ++this.state.unread[business.userId];
                firebase.database().ref('metadata/' + key).child('unread').update({[business.userId]: unread || 1});
            }
            let msgKey = firebase.database().ref('message').child(key).push(sendMessage).key;
            firebase.database().ref('metadata/' + key).update({lastMessageText: message[0] ? message[0].text : 'Sent an image', updatedAt: new Date()});
            firebase.database().ref('message').child(key).child(msgKey).update({'_id': msgKey});
        }

    setTypingStatus(roomId, uid){
        clearTimeout(typingTimer);
        var ref = firebase.database().ref('metadata/' + roomId).child('isTyping');
        ref.update({[uid]: true});
        typingTimer = setTimeout(() => {
            ref.update({[uid]: false});
        }, 3000)
    }

    renderSendButton(props) {
        return (
            <Send
                {...props}
            >
                <View style={{marginRight: 10, marginTop: 5}}>
                    <Image source={require('../../../assets/send.png')} resizeMode={'center'}/>
                </View>
            </Send>
        );
    }

    getImage(key){
        ImagePicker.openPicker({
            includeBase64: true,
            compressImageQuality : 0.5
        }).then(image => {console.log(image,"imageeeeeeeeeeeeeeeeee")
        // image.size < 2097152 ? this.messageSubmit([], image.data) :alert("Image size should not exceed 2MB!")
        this.messageSubmit([], image.data)    
    });
    }


    renderCustomActions(props) {
        const options = {}
        this.props.addresses.map(elem => {
            options['Send ' + elem.nickname.toUpperCase() + ' address'] = (props) => {
                this.setState({text: elem.businessAddress + ' ' + elem.address.description})
            }
        });
        options['Upload Image From Gallery'] = (props) => {
            this.getImage()
        };
        options['Upload Image From Camera'] = (props) => {
            this._openCamera()
        };
        options['Cancel'] = () => {}

        return (
            <Actions
                {...props}
                options={options}
            />
        );
    }


    renderFooter(props) {
        let {data} = this.props.navigation.state.params;
        if (this.state.typingText) {
            return (
                <View style={styles.footerContainer}>
                    <Text style={styles.footerText}>
                        {data.businessName || data.name} is Typing...
                    </Text>
                </View>
            );
        }
        return null;
    }

    getImageFromCamera(image) {
        this._closeCamera();
        this.messageSubmit([], image.data);
    }

    slideToggle(showAddresses) {
        this.setState({showAddresses});
        Animated.timing(                  // Animate over time
            this.state.slideAnim,            // The animated value to drive
            {
                toValue: showAddresses ? height - 80 : 86,                   // Animate to opacity: 1 (opaque)
                duration: 500,              // Make it take a while
            }
        ).start();                        // Starts the animation
    }


    render(){
        let { data, userToBusiness } = this.props.navigation.state.params;
        console.log(userToBusiness,data,"to be checked")
        let { user, userBusiness } = this.props;
        const images = data.images ? Object.values(data.images) : [];
        const addresses = _.map(data.address, address => address);
        const { text, messages, count, limit, roomId, showAddresses, slideAnim } = this.state;
        return(
            <View style = {{flex : 1, flexDirection: 'column', justifyContent: 'space-between', backgroundColor : "#fff", paddingTop: userToBusiness ? 0 : 100}}>
            
            {this.state.keyboardOpened ||
                <Animated.View style={{zIndex: 1, backgroundColor: 'white', position: userToBusiness ? 'relative' : 'absolute', height: slideAnim, width: '100%', flexDirection: 'column', justifyContent: 'space-between'}}>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                        <View style = {{flex: 2, marginTop :10,marginLeft : 10}}>
                            {userToBusiness && <View style={{flexDirection: 'row'}}>{data.categoriesSelected.map((data, index) =><Text style={{color: '#ababab', marginRight: 5}} key={index}>{data}</Text>)}</View>}
                            <View style={{flex: 1, flexDirection: 'column',marginBottom:-15}}>
                                {userToBusiness ? <Text style={{color: '#ababab'}}>Opens: {data.time && (data.time.time1 + ' - ' + data.time.time2)}</Text>
                                    :
                                    (addresses.length > 1) && !showAddresses ? <View>
                                        <Text style={{color: '#ababab', fontSize: 16, fontWeight: 'bold'}}>{addresses[addresses.length - 1].nickname.toUpperCase()} Address</Text>
                                        <Text numberOfLines={1} style={{color: '#ababab'}}>{addresses[addresses.length - 1].businessAddress + ' ' + addresses[addresses.length - 1].address.description}</Text>
                                        <TouchableOpacity onPress={this.slideToggle.bind(this, true)}>
                                            <Text style={{color: '#4990d0'}}>Show more addresses</Text>
                                        </TouchableOpacity>
                                    </View> :
                                        _.map(addresses, (elem, index) => {
                                            return <View style={{marginBottom: 15}}>
                                                <Text style={{color: '#ababab', fontSize: 16, fontWeight: 'bold',}}>{elem.nickname.toUpperCase()} Address</Text>
                                                <Text numberOfLines={1} style={{color: '#ababab'}}>{elem.businessAddress + ' ' + elem.address.description}</Text>
                                            </View>
                                        }).reverse()
                                }
                            </View>
                        </View>
                        <View style = {{flex: 1, marginTop :10,marginRight : 10}}>
                            <Text style={{color: '#ababab'}}>{userToBusiness ? 'Accepts:' : 'Phone'}</Text>
                            {userToBusiness ? <View style={{flexDirection: 'row'}}>{data.paymentsSelected.map((data, index) => <Text style={{color: '#ababab',marginLeft:3}} key={index}>{data}</Text>)}</View> :
                                <Text style={{color: '#ababab'}}>{data.phoneNumber}</Text>}
                        </View>
                    </View>
                    {showAddresses &&
                    <View>
                        <TouchableOpacity onPress={this.slideToggle.bind(this, false)}>
                            <Text style={{color: '#4990d0', textAlign: 'center'}}>Show less address</Text>
                        </TouchableOpacity>
                    </View>}

                </Animated.View>
}
                {user ? <View style = {{flex: 2, flexDirection : 'column', justifyContent: 'space-between'}}>
                    {this.state.keyboardOpened || <View>
                    {userToBusiness && !!images.length && <View><Text style = {{marginTop :1,marginLeft : 10}}>MENU</Text>
                        <View style={{flexDirection: 'row', justifyContent: 'space-around', marginTop: 2, marginBottom: 1, paddingTop: 5, paddingBottom: 5, borderTopColor: '#eaeaea', borderTopWidth: 0, borderBottomColor: '#eaeaea', borderBottomWidth: 1}}>
                            {
                                images.map((image, key)=>{console.log(image,"image")
                                    let img;
                                    if(image) {
                                        img = {opt: {uri: image.thumbUrl, width: 50, height: 50}}
                                    } else {
                                        img = {opt: require('../../../assets/images.png')}
                                    }
                                    return(<TouchableOpacity style={{borderWidth: 1, borderColor: '#ababab', borderRadius: 5}} onPress={() => this._openModal(image.thumbUrl)} key={key}><Image key={key.toString()}
                                                                                                                                                                                      style={{width: 70, height: 70, borderRadius: 8}}
                                                                                                                                                                                      source= {img.opt}/>
                                    </TouchableOpacity>)
                                })
                            }
                        </View>
                        
                        

                        </View>}
                        </View>}
                    <GiftedChat
                        text={this.state.text}
                        messages={messages}
                        onSend={(messages) => this.messageSubmit(messages)}
                        user={userToBusiness ? {_id: user.uid, name: user.displayName} : {_id: userBusiness._id, name: userBusiness.businessName}}
                        placeholder={userToBusiness ? ('Send order to' + " " + data.businessName) : ('Chat with ' + data.name)}
                        isAnimated={true}
                        renderAvatar={null}
                        loadEarlier={count > messages.length}
                        onLoadEarlier={() => {
                            this.getEarlierMessages(roomId, limit + 3)
                        }}
                        renderSend = {this.renderSendButton.bind(this)}
                        renderActions={this.renderCustomActions.bind(this)}
                        renderFooter={this.renderFooter.bind(this)}
                        renderBubble ={(message) => {
                            var isMe = message.user._id == message.currentMessage.user._id;
                            var style = {backgroundColor: isMe ? '#fafafa' : '#edf5fc', padding: message.currentMessage.image ? 5 : 20 , width: '60%', marginBottom: 15};
                            style[isMe ? 'borderTopLeftRadius' : 'borderTopRightRadius'] = 20;
                            style[isMe ? 'borderBottomLeftRadius' : 'borderBottomRightRadius'] = 20;
                            return <View style={style}>
                                        {!!message.currentMessage.image && <TouchableOpacity onPress={() => this._openModal(message.currentMessage.image)}>
                                            <Image style={{width: '100%', height: 150, borderRadius: 20}} source={{uri: message.currentMessage.image}}/></TouchableOpacity>}
                                        {!!message.currentMessage.text && <Text>{message.currentMessage.text}</Text>}
                                   </View>
                        }}
                        onInputTextChanged={(text) => {
                            this.setState({text});
                            roomId && this.setTypingStatus(roomId, user.uid)
                        }}
                    />
                </View>
                    :
                <View style={{ flex : 2}}>
                    <Login navigation={this.props.navigation}/>
                </View>
                }

                
                <Modal
                    visible={this.state.modalVisible}
                    animationType={'slide'}
                    onRequestClose={() => this._closeModal()}>
                        <TouchableOpacity
                            style={styles.closeButton}
                            onPress={() => this._closeModal()}><Text style={{color: 'white', fontSize: 18, fontWeight: 'bold'}}>X</Text>
                        </TouchableOpacity>
                    <ImageZoom cropWidth={Dimensions.get('window').width}
                               cropHeight={Dimensions.get('window').height}
                               imageWidth={Dimensions.get('window').width}
                               imageHeight={Dimensions.get('window').height}>
                        <View style={styles.innerContainer}>
                            <Image resizeMode='contain' style={{flex: 1, height: null, width: null}}
                                   source={{uri: this.state.preview}}/>
                        </View>
                    </ImageZoom>
                </Modal>
                <Modal
                    visible={this.state.cameraVisible}
                    animationType={'slide'}
                    onRequestClose={() => this._closeCamera()}>
                    <GullyCamera close={() => this._closeCamera()} getImageFromCamera={(image) => this.getImageFromCamera(image)}/>
                </Modal>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    closeButton: {
        alignItems: 'flex-start',
        backgroundColor: 'rgba(0, 0, 0, 0.99)',
        padding: 10
    },
    innerContainer: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'black',
    },

    container: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    autoTags: {
        backgroundColor: 'white',
        fontSize: 20
    },
    footerContainer: {
        marginTop: 5,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10,
    },
    footerText: {
        fontSize: 14,
        color: '#aaa',
    }
});

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        SEND_MESSAGE
    }, dispatch)
};

const mapStateToProps = (state) => {
    return {
        user: state.userReducer.user,
        userBusiness: state.storeReducer.userBusiness,
        addresses: state.userReducer.user && state.userReducer.user.addresses
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Info)