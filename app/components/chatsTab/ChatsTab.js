import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image, Dimensions
} from 'react-native';
const { width, height } = Dimensions.get('window');
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';

import Login from '../chatsTab/ChatsTab';
import ChatsList from '../chats/ChatsList'

export class ChatsTab extends Component{
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props);
    }

    login = (role) => {
        if(role == 'user'){

            this.props.navigation.navigate('Login', {role: null});
        }else if(role == 'business'){
            this.props.navigation.navigate('Login', {role: 'business'});
        }
    };

    render(){
        return(
            <View style={{flex: 1}}>
                {this.props.user ?
                    <ChatsList navigation={this.props.navigation}/> : (
                    <View style={{flex: 1, flexDirection: 'column', backgroundColor: 'white'}}>
                        <View style={{flex: 1, flexDirection: 'column'}}>


                            <Text style={{fontSize:18,padding:10,marginLeft:10,fontWeight:'bold'}}>Are you a user?</Text>
                            <Text style={{color: '#4990d0', fontSize: 18,padding:10,marginLeft:10}} onPress = {this.login.bind(this, 'user')}>
                                Signup with phone number
                            </Text>

                            <View style={{marginLeft:10}}>
                                <View style={{flexDirection:'row', flexWrap:'wrap',alignItems:'center'}}><Image style={{height:8,width:8,marginRight:5}} source={require('../../../assets/bullet.png')}/><Text style={{color:'#8a8f96'}}>Get discovered nearby</Text></View>
                                <View style={{flexDirection:'row', flexWrap:'wrap',alignItems:'center'}}><Image style={{height:8,width:8,marginRight:5}} source={require('../../../assets/bullet.png')}/><Text style={{color:'#8a8f96'}}>Increase customer base</Text></View>
                                <View style={{flexDirection:'row', flexWrap:'wrap',alignItems:'center'}}><Image style={{height:8,width:8,marginRight:5}} source={require('../../../assets/bullet.png')}/><Text style={{color:'#8a8f96'}}>Manage live chat orders</Text></View>
                            </View>

                            <Text style={{fontSize:18,padding:10,marginLeft:10,fontWeight:'bold'}}>existing user?</Text>

                            <Text style={{color: '#4990d0', fontSize: 15,padding:10,marginLeft:10}} onPress = {this.login.bind(this, 'user')}>
                                Login to your account
                            </Text>

                        </View>

                        <View style={{flex: 1,width: width,backgroundColor:'rgb(244,249,253)'}}>
                            <Text style={{fontSize:18,padding:10,marginLeft:10,fontWeight:'bold'}}>Are you a business?</Text>
                            <Text style={{color: '#4990d0', fontSize: 18,padding:10,marginLeft:10}}
                                  onPress = {this.login.bind(this, 'business')}>
                                Create your Business profile
                            </Text>
                            <View style={{marginLeft:10}}>
                                <View style={{flexDirection:'row', flexWrap:'wrap',alignItems:'center'}}><Image style={{height:8,width:8,marginRight:5}} source={require('../../../assets/bullet.png')}/><Text style={{color:'#8a8f96'}}>Get discovered nearby</Text></View>
                                <View style={{flexDirection:'row', flexWrap:'wrap',alignItems:'center'}}><Image style={{height:8,width:8,marginRight:5}} source={require('../../../assets/bullet.png')}/><Text style={{color:'#8a8f96'}}>Increase customer base</Text></View>
                                <View style={{flexDirection:'row', flexWrap:'wrap',alignItems:'center'}}><Image style={{height:8,width:8,marginRight:5}} source={require('../../../assets/bullet.png')}/><Text style={{color:'#8a8f96'}}>Manage live chat orders</Text></View>
                            </View>
                            <View>
                                <Image
                                    style={{width: width, height: height * 0.18}}
                                    source={require('../../../assets/gully-image-signup-chat-tab.png')}
                                />
                            </View>
                        </View>
                    </View>)
                }


            </View>
        )
    }
}

const styles = StyleSheet.create({

});

const mapStateToProps = (state) => {
    return {
        user: state.userReducer.user
    };
};

export default connect(
    mapStateToProps
)(ChatsTab)