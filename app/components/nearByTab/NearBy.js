
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Dimensions,
    Platform,
    TextInput,
    Image,
    FlatList,
    ScrollView
} from 'react-native';
const { width, height } = Dimensions.get('window');
import { UPDATE_LOCATION } from '../../redux/actions/locationAction';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import _ from 'underscore';
import Avatar from '../avatar/Avatar';
import geoFire from 'geofire';
import firebase from 'react-native-firebase';
import addressBook from '../addressBook/AddressBook'
import SvgUri from 'react-native-svg-uri';


export class NearBy extends Component{
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            error: false,
            searchCriteria: ''
        };
    }



    login = (role) => {
        if(this.props.user){
            this.props.screenProps.navigation.navigate('Profile');
        } else {
            this.props.screenProps.navigation.navigate('Login', {role: 'business'});
        }
    };

    calcDistance(calcDistance, myloc){
        return calcDistance.map((items)=>{
            var storeLoc = Object.values(items.address.address);
            storeLoc = [parseFloat(storeLoc[0]) , parseFloat(storeLoc[1])];
            items.distance = Number(geoFire.distance(myloc, storeLoc).toFixed(1));
            return items;
        });
    }


    search(searchCriteria){
        searchCriteria = searchCriteria.toLowerCase();
        let myLocation = Object.values(this.props.screenProps.location),
            categorySearched = false,
            searched = this.props.stores.filter((elem) => {
                return (elem.businessName.toLowerCase().search(searchCriteria) > -1) || _.find(elem.categoriesSelected, (category) => {
                        if(category.toLowerCase().search(searchCriteria) > -1){
                            if(category.toLowerCase() == searchCriteria){
                                categorySearched = true;
                            }
                            return true;
                        }

                    })
            });
        this.setState({searchData:  this.calcDistance(searched, myLocation), searchCriteria, loading: false, categorySearched});

    }


    gotoChat(item){
        this.props.screenProps.navigation.navigate('Info', {data: item, userToBusiness: true});
    }
    render(){
        var currentUID=firebase.auth()

        var {loading, error, searchCriteria, searchData, data, categorySearched} = this.state;
        var {stores, userBusiness} = this.props;

        return(

                <View style={styles.container}>
                    <ScrollView>
                    <TextInput
                        ref="input"
                        style={{height: 55, backgroundColor: '#efefef', borderBottomWidth: 2, borderColor: '#cecece', paddingLeft: 15}}
                        caretHidden = {false}
                        underlineColorAndroid="transparent"
                        placeholder="Filter nearby stores by name, categories tags..."
                        onChangeText={searchCriteria => this.search(searchCriteria)}
                        value={searchCriteria}
                    />
                    <View style={{height: 40, borderBottomWidth: 1, justifyContent: 'center', paddingLeft: 15, backgroundColor: 'white', borderColor: '#efefef'}}>
                        <Text>
                            <Text style={{fontSize: 16, fontWeight: 'bold', fontStyle: 'italic'}}>i </Text>
                            Displaying
                            <Text style={{fontWeight: 'bold'}}> all {searchCriteria.length ? searchData.length : stores && stores.length} {categorySearched && searchCriteria + ' '}stores </Text>within
                            <Text style={{fontWeight: 'bold'}}> 20 kms</Text> in 1.1s
                        </Text>
                    </View>
                        <FlatList
                            keyExtractor={(item, index) => index}
                            data={(searchCriteria && searchCriteria.length) ? Object.values(searchData) : stores}
                            renderItem={({item}) =>
                          <TouchableOpacity style={styles.touchBorder} onPress={() => {
                                    return (
                                    this.props.user && this.props.user._user.uid == item.userId ? this.props.screenProps.navigation.navigate("ChatsList",{data:item}) :
                                    this.gotoChat(item)
                                    )
                                }}>
                                   <View style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    alignItems: 'stretch',
                                    // alignContent:"center"
                                   }}>
                                   <View>
                                    <View style={{flex: 1, flexDirection: 'row',  height: 35}}>
                                        <Avatar svg={true} category={item.categoriesSelected}
                                        containerStyle={{justifyContent: 'center',alignItems:"center",marginLeft:5,marginTop:8,marginBottom:8}}/>
                                        {this.props.userPressence && this.props.userPressence[item.userId] ? 
                                        <Image source={require('../../../assets/online.png')} style={{
                                            width: 9,
                                            height: 9,
                                            marginLeft: -9,
                                            marginTop: 50,
                                            alignSelf: 'center',
                                            borderColor: 'white',
                                            borderWidth: 2,
                                            borderRadius: 10,
                                        }}/> :
                                        <Image source={require('../../../assets/offline.png')} style={{
                                            width: 9,
                                            height: 9,
                                            marginLeft: -9,
                                            marginTop: 50,
                                            alignSelf: 'center',
                                            borderColor: 'white',
                                            borderWidth: 2,
                                            borderRadius: 10,
                                        }}/> }

                                        <Text style={styles.businessName}>{item.businessName.toUpperCase()}</Text>

                                      </View>

                                        <View style={{
                                            flex: 1,
                                            flexDirection: 'row',
                                            marginLeft:40
                                            }}>
                                            <Text style={{color: '#a2a2a2', fontSize: 15, paddingLeft: 10,marginLeft:10}}>{item.distance + ' km'}</Text>
                                            {item.categoriesSelected && item.categoriesSelected.filter((elem, index) => index < 2).map((data, index) => (<TouchableOpacity onPress={this.search.bind(this, data)} key={'cat_'+index}><Text style={styles.item} key={index}>{data}</Text></TouchableOpacity>))}
                                            {item.categoriesSelected && (item.categoriesSelected.length > 2) ? <Text>...</Text> : null}
                                        </View>
                                    </View>
                                    <View style={{flex: 1,flexDirection: 'column',justifyContent: 'flex-end', alignItems: 'flex-end', paddingRight: 20}}>
                                        <Text style={{fontSize: 30, color: '#cecece'}}> > </Text>
                                        <View style={{flexDirection:'row',flexWrap:'wrap'}}>{item.paymentsSelected && item.paymentsSelected.filter((elem, index) => index < 2).map((data, index) => (<Text style={styles.item} key={index}>{data}</Text>))}</View>
                                        {item.paymentsSelected && item.paymentsSelected.length > 2 ? <Text>...</Text> : null}
                                    </View>
                                   </View>
                          </TouchableOpacity>


                    }
                        />
                </ScrollView>

                    <View style={{flexDirection: 'row',justifyContent : 'space-between', alignItems: 'center',height: 70,width: width}}>
                            <View  style = {{ marginLeft : 20}} >
                                <Text style = {{fontWeight: 'bold', marginTop : 20, color: '#4990d0'}} onPress = {this.login.bind(this, 'business')}>
                                    {this.props.user ? (userBusiness && Object.keys(userBusiness).length ? 'Edit business profile' : 'Add business profile') : 'signup as business/service' }
                                </Text>
                            </View>
                            <View  style = {{marginRight : 20}}><Text style = {{fontWeight: 'bold', marginTop : 20, color: '#4990d0'}}>
                                {this.props.user ? 'Send feedback' : 'term & privacy'}</Text></View>
                    </View>

                </View>

        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        backgroundColor: 'white',
    },
    button: {
        marginTop: 20,
        height: 40,
        width: 80,
        backgroundColor: 'orange',
        justifyContent: 'center'
    },

    businessName: {
        fontWeight: '100',
        padding: 10,
        fontSize: 16,
        height: 55,
    	color: '#333333'
},
    item: {
        fontSize: 12,
        color: '#4F4F4F',
        paddingLeft: 10,
        paddingTop: 2,
        textDecorationLine: 'underline'
    },
    flat: {
        flex: 1,
        backgroundColor: 'red',
        paddingTop: 22
    },
    touchBorder: {
        borderBottomWidth: 1,
        paddingBottom: 10,
        borderColor: '#eaeaea'
    }
});

const mapStateToProps = (state) => {
    return {
        location: state.locationReducer.location,
        stores: state.storeReducer.stores,
        user: state.userReducer.user,
        userBusiness : state.storeReducer.userBusiness,
        userPressence: state.userReducer.userPressence
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        UPDATE_LOCATION
    }, dispatch)
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NearBy)