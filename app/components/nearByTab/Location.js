import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Dimensions,
    Image,
    Platform,
    Button
} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { UPDATE_LOCATION } from '../../redux/actions/locationAction';
import { UPDATE_STORE, LISTEN_PRESENCE } from '../../redux/actions/storeAction';
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';
import NearBy from "./NearBy";
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";


const { width, height } = Dimensions.get('window');


class Location extends Component{

    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            error: false
        }
    }




    componentDidMount(){
        if(Platform.OS === 'android'){
            this.allowLocation();
        } else {
            this.getCurrentLocation();
        }
    }

    componentDidUpdate(prevProps){
        if(this.props.stores && this.props.location){
            this.props.LISTEN_PRESENCE();
            this.props.navigation.navigate('Home');
        }
    }

    componentWillMount(){
        FCM.on(
            FCMEvent.Notification, async (notif) => {
                // there are two parts of notif. notif.notification contains the notification payload, notif.data contains data payload
                console.log('notif..................................', notif)
                if(notif.local_notification){
                    //this is a local notification
                }
                //
                if(notif.opened_from_tray){
                        try{
                            notif.screen == "ChatsList" ? this.props.navigation.navigate("ChatsList") : this.props.navigation.navigate("Info",{data: notif.chatObj, userToBusinesses: notif.userToBusinesses})
                        }
                        catch(error){
                            console.log(error)
                        }
                    //iOS: app is open/resumed because user clicked banner
                    //Android: app is open/resumed because user clicked banner or tapped app icon
                }
                // await someAsyncCall();
            }
        );

    }


    allowLocation(){
        LocationServicesDialogBox.checkLocationServicesIsEnabled({
            message: "<h2>Use Location?</h2> \
                            This app wants to change your device settings:<br/><br/>\
                            Use GPS for location<br/><br/>",
            ok: "OK",
            cancel: "CANCEL"
        }).then((success) => {
            this.setState({loading: true, error: null});
            setTimeout(() => {this.getCurrentLocation()}, 5000);
        }).catch((error) => {
            this.setState({
                error: 'You need to allow location in order to proceed'
            });
        });
    }

    getCurrentLocation(){
        navigator.geolocation.getCurrentPosition(
            (position) => {
                var {longitude, latitude} = position.coords;
                this.props.UPDATE_LOCATION({longitude, latitude});
                this.props.UPDATE_STORE({longitude, latitude});
            },
            (error) => {
                setTimeout(() => {this.getCurrentLocation()}, 5000);
            }
        );
    }

    render(){
        var {loading, error} = this.state,
            {stores} = this.props;
        return(
            <View style={styles.container}>
                <View style = {{marginTop : 50, alignItems: 'center'}}>
                        <Image
                            style={{marginBottom: 4, width: width * 0.35, height: height * 0.04}}
                            source={require('../../../assets/gully.png')}
                        />
                    <View style={{ borderTopWidth: 2, width: width * .5,borderColor: '#4F8FC9',marginBottom:2}}></View>
                    <View style={{alignItems: 'center', borderTopWidth: 2, width: width * .5,borderColor: '#4F8FC9'}}>
                        <Text style={{fontSize: 13,fontWeight:'bold',color:'#8a8f96'}}>chat with nearby local shops</Text>
                        <Text style={{fontSize: 13,fontWeight:'bold',color:'#8a8f96'}}>get thing done</Text>
                    </View>
                </View>
                <View style={{alignItems: 'center'}}>
                    {loading && <Image style={{height: 50, width: 50}} source={require('../../../assets/loader.gif')}/>}
                    {loading && <Text style={{marginTop: 10, color: '#4F8FC9'}}>LOADING NEARBY STORES...</Text>}
                    {error && <Text>{error}</Text>}
                    {error && <View style={{marginTop: 10}}><Button
                        onPress={() => this.allowLocation()}
                        title="ALLOW LOCATION"
                        color="#4990d0"
                    /></View>}
                </View>
                <View style = {{marginBottom : 30}}>
                    <Text style={[styles.center, styles.textWidth ]}>TAP 'OK' to allow location access to load nearby shops...</Text>
                </View>
                <View>
                    <Image
                        style={{width: width, height: height * 0.18}}
                        source={require('../../../assets/gully-img-loading-page.png')}
                    />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex : 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor : '#ffffff'
    },
    center: {
        textAlign: 'center'

    },
    textWidth: {
        width: width * 0.5,
        fontSize: 11,
        color: '#4F8FC9'
    },
    button: {
        height: 40,
        width: 80,
        backgroundColor: 'orange',
        justifyContent: 'center',
        marginBottom : 40
    }
});

const mapStateToProps = (state) => {
    return {
        location: state.locationReducer.location,
        stores : state.storeReducer.stores
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        UPDATE_LOCATION,
        UPDATE_STORE,
        LISTEN_PRESENCE,
    }, dispatch)
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Location)
