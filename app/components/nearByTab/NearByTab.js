
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Dimensions,
    Platform,
    TextInput,
    FlatList,
    TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { UPDATE_USER,UPDATE_USER_ADDRESS,UPDATE_USER_PRESSENCE } from '../../redux/actions/userAction';
import { LISTEN_CATEGORIES, LISTEN_PAYMENTS, GET_USER_BUSINESS } from '../../redux/actions/storeAction';
import NearBy from "./NearBy";
import Location from "./Location";
import { bindActionCreators } from 'redux';

import firebase from 'react-native-firebase';
const { width, height } = Dimensions.get('window');
import { NavigationActions } from 'react-navigation';
import _ from 'underscore';

export class NearByTab extends Component{

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            searchData: [],
            searchCriteria: ''
        };
         props.UPDATE_USER_PRESSENCE()
         props.LISTEN_CATEGORIES();
         props.LISTEN_PAYMENTS();
         firebase.auth().onAuthStateChanged((user) => {
             if(!user){
                 this.props.UPDATE_USER(user);
                 props.navigation.navigate('DrawerClose');
                 props.navigation.dispatch({type: 'Reset', index: 0, actions: [{ type: 'Navigate', routeName:'ChatsTab'}]});
                 props.navigation.navigate('NearByTab')
             } else {
                 props.GET_USER_BUSINESS(user.uid);
                 var amOnline = firebase.database().ref('.info/connected');
                 var userRef = firebase.database().ref('presence/' + user.uid);
                 amOnline.on('value', function(snapshot) {
                     if (snapshot.val()) {
                         userRef.onDisconnect().remove();
                         userRef.set(true);
                     }
                 });
                 this.updateUserState(user);
             }
         });
     }

     updateUserState(user){
         const CurrentUID = user.uid;
         firebase.database().ref("users").child(CurrentUID).child("address")
             .on("value",(snap)=>{
                 var data = snap.val();
                 var arr = _.map(data, (address, key) => {
                     address._id = key;
                     return address;
                 });
                 user.addresses = arr;
                 user.phoneNumber = user._user.phoneNumber;
                 this.props.UPDATE_USER_ADDRESS(user)
             })
     }

    render(){
        return (
                <View style = {{flex : 1}}>
                    <NearBy screenProps={{...this.props}}/>
                </View>
            )
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        UPDATE_USER,
        UPDATE_USER_ADDRESS,
        UPDATE_USER_PRESSENCE,
        LISTEN_CATEGORIES,
        LISTEN_PAYMENTS,
        GET_USER_BUSINESS
    }, dispatch)
};

const mapStateToProps = (state) => {
    return {
        location: state.locationReducer.location
    };
};


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NearByTab)