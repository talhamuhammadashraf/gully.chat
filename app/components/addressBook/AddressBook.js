
import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    Image,
    TextInput,
    View,
    Button,
    Picker,
    TouchableOpacity,
    Dimensions,
    ScrollView,
    Alert,
    Keyboard,
    FlatList

} from 'react-native';
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {UPDATE_USER_ADDRESS} from '../../redux/actions/userAction'
import firebase from 'react-native-firebase';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';


const GooglePlacesInput = (props) => {
    return (
        <GooglePlacesAutocomplete
            
            placeholder={props.businessAddress}
            minLength={2} // minimum length of text to search
            autoFocus={false}
            returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
            textInputProps={{onFocus: () => props.onFocus()}}
            fetchDetails={true}
            renderDescription={row => row.description} // custom description render
            onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true

                    props.update(data.description);
            }}

            getDefaultValue={() => props.defaultValue?props.defaultValue:""}

            query={{
        // available options: https://developers.google.com/places/web-service/autocomplete
        key: 'AIzaSyA_B8Bx2QlzoP31krA92nWYvbwbg29MVks',
        language: 'en', // language of the results
        types: 'geocode' // default: 'geocode'
      }}

        styles={{
        textInputContainer: {
            width: '100%',
            backgroundColor: '#fafafa',
            borderTopWidth: 0,
            borderBottomWidth: 0,
            borderLeftWidth: 0,
            borderRightWidth: 0,
            height: 70
        },
        row:{
          backgroundColor: '#fafafa'
        },
        textInput: {
            backgroundColor: '#fafafa',
            fontSize: 20,
            height: 50,
            marginLeft: 5
        },
        description: {
          backgroundColor: '#fafafa',
          fontSize: 20,
          paddingLeft: 3,
          paddingTop: 4,
          paddingBottom: 4,
          height: 35
        },
        container: {
            flex: 0,
            borderTopColor: '#efefef',
            borderBottomColor: '#efefef',
            borderTopWidth: 1,
            borderBottomWidth: 1
        }
      }}
            nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
            GoogleReverseGeocodingQuery={{
        // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
      }}
            GooglePlacesSearchQuery={{
        // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
        rankby: 'distance',
        types: 'food'
      }}

            filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities

            debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
            />
    );
};

const { width, height } = Dimensions.get('window');

class AddressBook extends Component{
    static navigationOptions = {
        header: null
    };

    constructor(props){
        super(props);
        this.state={
            businessAddress:"",
            address :"",
            nickname:""
        }
    }

    save(){
        let payload={
            businessAddress:this.state.businessAddress,
            address:this.state.address,
            nickname:this.state.nickname
        };
        const CurrentUID = firebase.auth().currentUser.uid;
        let request = this.state.itemID ?
            firebase.database().ref("users").child(CurrentUID).child("address").child(this.state.itemID).update(payload) :
            firebase.database().ref("users").child(CurrentUID).child("address").push(payload);
        request.then(()=>{
            this.setState({
                keyboardOpened:false,
                businessAddress:"",
                address :"",
                nickname:"",
                editMode: false,
                itemID: ''
            });
            this.refs.scrollView.scrollTo();
        });
    }
    componentWillMount () {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
    }

    componentWillUnmount () {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow () {
        this.setState({ keyboardOpened: true })
    };

    _keyboardDidHide () {
        this.setState({ keyboardOpened: false })
    };

    _renderModalContent = () => (
        <View style={styles.modalContent}>
            {!this.state.editMode && <TouchableOpacity onPress={() => this.update(this.state.itemID)}>
                <View style={styles.button}>
                    <Text style={{color: 'white'}}>EDIT</Text>
                </View>
            </TouchableOpacity>}
            {!this.state.editMode && <TouchableOpacity onPress={() => this.delete(this.state.itemID)}>
                <View style={styles.button}>
                    <Text style={{color: 'white'}}>DELETE</Text>
                </View>
            </TouchableOpacity>}
            {this.state.editMode && <TouchableOpacity onPress={() => this.cancel()}>
                <View style={styles.button}>
                    <Text style={{color: 'white'}}>CANCEL</Text>
                </View>
            </TouchableOpacity>}
        </View>
      );
    delete(id) {

        const CurrentUID = firebase.auth().currentUser.uid;
        firebase.database().ref("users").child(CurrentUID).child("address").child(id).remove()
    }
    update(id){
        const CurrentUID = firebase.auth().currentUser.uid;
        firebase.database().ref("users").child(CurrentUID).child("address").child(id).once("value", (snap) => {
            var data = snap.val()
            this.setState({
                businessAddress: data.businessAddress,
                address: data.address,
                nickname: data.nickname,
                itemID: id,
                editMode: true
            })
        })
        .then(()=>{this.refs.scrollView.scrollToEnd()})
    }
    cancel(){
        this.setState({
            businessAddress:"",
            address :"",
            nickname:"",
            editMode: false,
            itemID: ''
        });
    }
    render(){
        const {address, businessAddress, nickname, editMode} = this.state;
        let disabled = !(address && businessAddress && nickname) || !editMode && (this.props.addresses && this.props.addresses.length >= 4);
        return(
            <View style={styles.container}>
                <ScrollView ref="scrollView" scrollEnabled={true}>

                    <View style={{height: 40, borderBottomWidth: 1, justifyContent: 'center', backgroundColor: 'white', borderColor: '#efefef'}}>
                        <Text style={{fontSize: 18, fontWeight: 'bold', color: '#333333', textAlign: 'center'}}>Address Book</Text>
                    </View>

                    {this.props.addresses && !!this.props.addresses.length && <View>
                        <View style={{height: height*0.07, borderBottomWidth: 0.5, justifyContent: 'center', backgroundColor: 'white', borderColor: '#efefef'}}>
                            <Text style={{paddingLeft: 15, fontSize: 18, color: '#333333'}}>ADDRESS LIST</Text>
                        </View>
                        <View>
                            <FlatList
                                scrollEnabled={true}
                                data={this.props.addresses}
                                extraData={this.state}
                                renderItem={
                                    ({item})=>{

                                         return (
                                             <View style={{flex: 1, flexDirection: 'row', borderBottomWidth: 1, marginBottom:1, borderColor: '#efefef', paddingLeft: 15}}>
                                                <View style={{flex: 1}}>
                                                <TouchableOpacity onPress={()=>{this.setState({itemID: item._id})}}>
                                                    <Text style={{marginTop: 10, fontWeight:"bold"}}>{item.nickname}</Text>
                                                    <Text>{item.businessAddress}</Text>
                                                    <Text style={{marginBottom: 10}}>{item.address.description?item.address.description:"No city defined"}</Text>
                                                </TouchableOpacity>
                                                </View>
                                             {(this.state.itemID == item._id) && this._renderModalContent()}
                                             </View>
                                         );
                                       }
                                }
                            />
                        </View>
                    </View>}

                    <View style={{ justifyContent: 'center', backgroundColor: 'white', marginTop: 25}}>
                        <Text style={{paddingLeft: 15, fontSize: 17, borderBottomWidth: 0.5, color: '#333333',borderColor: '#efefef'}}>ADD NEW ADDRESS</Text>

                        <Text style={{paddingLeft: 15, color:"#c1bfbf"}}>I add address for quick reference in chats</Text>

                        <Text>{"\n"}</Text>

                        <Text style={{paddingLeft: 15, marginTop: 20, color: '#ababab'}}>FULL ADDRESS</Text>
                        <TextInput style={styles.textInput} defaultValue={this.state.businessAddress} placeholder="Your Address" onChangeText={(address)=>{this.setState({businessAddress:address})}} />
                        <GooglePlacesInput
                            businessAddress = {this.state.address.description || "Enter your city, state, country"}
                            update={(description) => this.setState({ address: { description: description, lat: this.props.location.latitude, lng: this.props.location.longitude } })}
                            onFocus={(description) => {
                                setTimeout(() => {
                                     this.refs.scrollView.scrollToEnd()
                                }, 1000)
                            }}
                        />

                        <Text>{"\n"}</Text>

                        <Text style={{paddingLeft: 15, marginTop: 25, color: '#ababab'}}>ADDRESS NICKNAME</Text>
                        <TextInput style={styles.textInput}  defaultValue={this.state.nickname} placeholder="Nickname" onChangeText={(nickname)=>{this.setState({nickname: nickname})}} />

                        <Text style={{paddingLeft: 15, color:"#c1bfbf", fontSize: 12, marginBottom: 10}}>Set an address nickname like HOME, OFFICE, FRIEND'S HOUSE</Text>
                    </View>

                </ScrollView>
                    {this.state.keyboardOpened || 
                <View style={{alignItems: 'center', justifyContent: 'center', backgroundColor: disabled ? "gray" : "#4990d0", height: 50, width: width}}>
                    <TouchableOpacity disabled={disabled} style = {{ width : 400}} onPress={this.save.bind(this)}>
                        <Text style={styles.footerText}>{this.state.editMode ? 'UPDATE' : 'SAVE'} ></Text>
                    </TouchableOpacity>
                </View>}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        backgroundColor: 'white'
    },
    footer: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "#4990d0",
        height: 50,
        width: width
    },
    footerText: {
        color: "white",
        fontSize: 25,
        fontWeight: 'bold',
        textAlign : 'center'
    },
    textInput: {
        height: 60,
        // marginTop:32,
        paddingLeft: 15,
        width: width,
        fontSize: 20,
        borderWidth: 1,
        backgroundColor: '#fafafa',
        borderColor: '#efefef'
    },
    button: {
        backgroundColor: '#4990d0',
        color: 'white',
        padding: 12,
        margin: 16,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        width: '80%',
        borderColor: 'rgba(0, 0, 0, 0.1)',
      },
    modalContent: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4
      }
});
const mapStateToProps = (state) => {
    return {
        location: state.locationReducer.location,
        addresses: state.userReducer.user.addresses
    };
};

const mapDispatchToProps =(dispatch) =>{
return bindActionCreators({
    UPDATE_USER_ADDRESS
},
dispatch)
}

export default connect(
    mapStateToProps,mapDispatchToProps
)(AddressBook)