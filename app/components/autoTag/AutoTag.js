import React, { Component } from "react";
import {
    View,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    TouchableHighlight,
    Dimensions,
    ScrollView
} from "react-native";
import Autocomplete from "react-native-autocomplete-input";
const {height, width} = Dimensions.get('window');

export default class AutoTags extends Component {
    constructor(props){
        super(props);
        this.state = {
            queries: [],
            text: '',
            newText: ''
                }
    }


    componentDidMount(props){

            this.updateText(this.props.tagsSelected);
    }


    componentDidUpdate(prevProps){
        if((prevProps.tagsSelected.length != this.props.tagsSelected.length) || (prevProps.tagsSelected[0] != this.props.tagsSelected[0])){
            this.updateText(this.props.tagsSelected, prevProps.tagsSelected.length != this.props.tagsSelected.length)
        }
    }

    updateText(tagsSelected, emptyQuery){
        tagsSelected = tagsSelected.join(',');
        var update = {text: tagsSelected};
        console.log(tagsSelected,'tagsselected**********');
        console.log(update,'update**********');
        emptyQuery && (update.queries = []);
        this.setState(update);
    }

    search(text) {
        if(!text){
            this.setState({queries: [], text, newText: text});
            return;
        }
        let {tagsSelected} = this.props;
        let queries;
        console.log('queries b', queries);
        let textArray = text.split(',');
        let newText = (textArray[textArray.length - 1]).trim().toLowerCase();
        queries = this.props.suggestions.filter((item) => {
            return (item.name.toLowerCase().indexOf(newText) > -1) && !this.props.tagsSelected.includes(item.name)
        });
        console.log('queries b', queries);
        this.setState({newText, text, queries});
    };

    handleAddition(name){
        this.setState({showSuggestions: false});
        this.props.handleAddition(name)
    }

    addTag(name){
        this.setState({showSuggestions: false});
        this.props.addTag(name)
    }

    render() {
        const { queries, text, newText } = this.state;
        let {tagsSelected} = this.props;
        tagsSelected = tagsSelected.join(',');
        return (
            <View style={styles.autoTag}>
                <TextInput
                    style={styles.textInput}
                    onFocus={() => this.setState({showSuggestions: true})}
                    onChangeText={(text) => {
                        this.search(text);
                        this.props.updateAutoTag(text)
                    }}
                    value={text}
                />
                {this.state.showSuggestions && !!newText.length && <ScrollView style={styles.tags}>
                    {queries.length ? (queries.map((item) => {
                        return (
                            <TouchableOpacity onPress={() => this.handleAddition(item.name)}>
                                <Text style={styles.tag}>{item.name}</Text>
                            </TouchableOpacity>
                        )
                    })) : (newText.length > 1) && <TouchableOpacity onPress={() => this.addTag(newText)}>
                        <Text style={styles.tag}>Add this tag</Text>
                    </TouchableOpacity>}
                </ScrollView>}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    textInput: {
        height: 60,
        marginTop: 8,
        paddingLeft: 15,
        width: width,
        fontSize: 20,
        borderWidth: 1,
        backgroundColor: '#fafafa',
        borderColor: '#efefef'
    },
    tags: {
        borderWidth: 1,
        borderColor: '#E0E0E0',
        position: 'absolute',
        backgroundColor: '#fafafa',
        width: '100%',
        top: 70,
        zIndex: 1,
        maxHeight: 160
    },
    tag: {
        fontSize: 20,
        borderBottomWidth: 1,
        borderColor: '#E0E0E0',
        marginBottom: 5,
        paddingLeft: 15,
        paddingTop: 4,
        paddingBottom: 4,
        width: '100%'
    },
    containerStyle: {
        minWidth: 200,
        maxWidth: 300
    }
});
