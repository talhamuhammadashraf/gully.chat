import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Dimensions,
    TouchableOpacity,
    Image
} from 'react-native';
import { connect } from 'react-redux';

const { width, height } = Dimensions.get('window');

class Header extends Component{
    constructor(props){
        super(props);
    }

    goBack(){
        this.props.goBack();
    }

    render(){
        var {business, userPressence, userToBusiness} = this.props;
        var data = business ? this.props.userBusiness : this.props.data;
        return (
        <View style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            backgroundColor: 'white' //
            }}>
            <View style={{width: 55}}>{this.props.goBack && <TouchableOpacity onPress={this.goBack.bind(this)}><Text style={{fontSize: 18, margin: 15, fontWeight: 'bold'}}>{'<'}</Text></TouchableOpacity>}</View>
            <View style={{width: width * 0.6, alignItems: 'center', justifyContent: 'center'}}>
                {data.businessName || data.name ? <View style={{alignItems: 'center'}}>
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                        {userPressence[business ? data.userId : (userToBusiness ? data.userId : data.ownerId)] ? <Image source={require('../../../assets/online.png')} style={{width: 10, height: 10, marginRight: 5}}/> :
                        <Image source={require('../../../assets/offline.png')} style={{width: 10, height: 10, marginRight: 5}}/> }
                        <Text style={{fontSize: 18, fontWeight: 'bold', color: '#333333'}}>
                        {
                            (data.businessName || data.name).toUpperCase()
                        }
                        </Text>
                    </View>
                    {data.time && <Text style={{fontSize: 12, color: '#ababab'}}>{data.time.time1 + ' -'}  {data.time.time2}</Text>}
                    {!data.time && <Text style={{fontSize: 12, color: '#ababab'}}>2 kms away</Text>}
                    </View>:
                <View style={{alignItems: 'center'}}>
                    <Image style={{width: width * 0.35, height: height * 0.04}} source={require('.. /../../assets/gully.png')}/>
                    {this.props.subtitle && <Text style={{marginTop: 5, fontSize: 13, color: '#ababab'}}>{this.props.subtitle.toUpperCase()}</Text>}
                </View>
                }
            </View>
            <View style={{height: 50, width: 55}}>
                {!this.props.user ? <Text></Text> :
                    <TouchableOpacity style={{width: 60}} onPress={() => this.props.navigate('DrawerOpen')}>
                        <Image
                            style={{width: 30, height: 30, marginTop: 8, marginLeft: 10}}
                            source={require('../../../assets/menu.png')}/>
                    </TouchableOpacity>
                }
            </View>
        </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.userReducer.user,
        userBusiness: state.storeReducer.userBusiness,
        userPressence: state.userReducer.userPressence
    };
};

export default connect(
    mapStateToProps
)(Header)