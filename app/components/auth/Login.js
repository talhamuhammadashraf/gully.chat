import React, { Component } from 'react';
import { View, Button, Text, TextInput, Image, Dimensions, TouchableOpacity, ScrollView, StyleSheet, Keyboard} from 'react-native';
import firebase from 'react-native-firebase';
import { UPDATE_USER } from '../../redux/actions/userAction';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Profile from '../profile/Profile';
import Chat from '../chats/Chat';
import PhoneInput from 'react-native-phone-input'
import SvgUri from 'react-native-svg-uri';
import {Platform} from 'react-native';
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';

FCM.on(FCMEvent.RefreshToken, (token) => {
    console.log(token)
    // fcm token may not be available on first load, catch it here
});

const { width, height } = Dimensions.get('window');

var interval;
export class Login extends Component {
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props);
        var database = firebase;
        this.unsubscribe = null;
        this.state = {
            loading: false,
            user: null,
            message: '',
            codeInput: '',
            phoneNumber: '+92',
            name: "",
            isConfirmed: false,
            confirmResult: null,
            timer: 30
        };
    }

    componentWillMount () {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
    }

    componentWillUnmount () {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow () {
        this.setState({ keyboardOpened: true })
    };

    _keyboardDidHide () {
        this.setState({ keyboardOpened: false })
    };

    componentDidMount(){
        console.log('Login..................')
        // iOS: show permission prompt for the first call. later just check permission in user settings
        // Android: check permission in user settings
        FCM.requestPermissions().then(()=>console.log('granted')).catch(()=>console.log('notification permission rejected'));

        FCM.getFCMToken().then(token => {
            console.log(token,"tokenLogin");
            this.setState({token});
            // store fcm token in your server
        });

        this.notificationListener = FCM.on(FCMEvent.Notification, async (notif) => {
            // optional, do some component related stuff

            console.log('notificationListener', notif)
        });

        // initial notification contains the notification that launchs the app. If user launchs app by clicking banner, the banner notification info will be here rather than through FCM.on event
        // sometimes Android kills activity when app goes to background, and when resume it broadcasts notification before JS is run. You can use FCM.getInitialNotification() to capture those missed events.
        // initial notification will be triggered all the time even when open app by icon so send some action identifier when you send notification
        FCM.getInitialNotification().then(notif => {
            console.log('getInitialNotification')
            console.log(notif)
        });

        firebase.auth().onAuthStateChanged((user) => {
            if(user){
                let database = firebase.database();
                database.ref('users/' + user.uid).update({
                    userId: user.uid,
                    name: this.state.name,
                    phoneNumber: this.state.phoneNumber,
                    verify: user._auth.authenticated,
                    deviceID: this.state.token
                });
                if(role = this.props.navigation.state.params.role) {
                    database.ref('users/' + user.uid).update({
                        role: role
                    });
                }
                user.updateProfile({
                    displayName: this.state.name || user.displayName
                }).then(() => {
                    this.props.UPDATE_USER(user, role);
                    clearInterval(interval);
                    this.setState({isConfirmed: true})
                });
            }
        });
    }

    componentDidUpdate(prevProps, prevState){
        const {isConfirmed} = this.state;
        const {user} = this.props;
        let role = this.props.navigation.state.params.role || (user ? user.role : '');
        if(user && prevProps.user != user || isConfirmed != prevState.isConfirmed){
            isConfirmed && ((role == 'business') ? this.navigateTo()
                : this.props.navigation.navigate('ChatsTab'))
        }
    }

    signIn = () => {
        const { phoneNumber, name } = this.state;
        var numberError = phoneNumber.length < 10,
            nameError = name.length < 2;
        this.setState({numberError, nameError});
        if(!numberError && !nameError){
            this.setState({ timer: 30,loading: true, message: 'Sending code ...'});
            firebase.auth().signInWithPhoneNumber(phoneNumber)
                .then(confirmResult => {
                    this.interval();
                    this.setState({ error: false, loading: false, confirmResult, message: 'Code has been sent!'})
                })
                .catch(error =>{
                    this.setState({ error, message: `Sign In With Phone Number Error: ${error.message}`, loading: false })
                })
        }

    };
    confirmCode = () => {
        this.setState({loading : true});
        const { codeInput, confirmResult } = this.state;
        if (confirmResult && codeInput.length) {
            confirmResult.confirm(codeInput)
                .then((user) => {
                    this.setState({ error: false, isConfirmed: true, message: 'Code Confirmed!' });
                    clearInterval(interval);
                })
                .catch(error => this.setState({ error, loading: false ,message: `Code Confirm Error: ${error.message}` }));
        }
    };

    renderPhoneNumberInput() {
        const { phoneNumber, keyboardOpened, name, nameError, numberError } = this.state;
        return (
            <View style={{flex: 1, flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center', backgroundColor: 'white'}}>
                <ScrollView>

                    <View style={{alignItems: 'center',padding:10,marginTop:5}}>
                        <SvgUri width="70" height="70" svgXmlData='<?xml version="1.0" encoding="iso-8859-1"?> <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"viewBox="0 0 490.524 490.524" style="enable-background:new 0 0 490.524 490.524;" xml:space="preserve"><g><g><path style="fill:#2C2F33;" d="M358.023,255.815c-5-2.2-10.8,0-13.1,5.1c-8.5,19.1-20.6,36-36.1,50.2c-2,1.9-3.2,4.5-3.2,7.3			v122.6h-169.8v-36.8c0-2.9-1.3-5.6-3.4-7.5c-2.2-1.9-5.1-2.7-7.9-2.3l-45.5,6.8c-12.8-0.4-23.2-10.9-23.2-23.8v-59.3			c0-5.5-4.4-9.9-9.9-9.9h-15.4c-4.9,0-8.6-1.4-10-3.6c-1.4-2.3-0.9-6.2,1.4-10.5l32.7-62.4c0.7-1.4,1.1-3,1.1-4.6v-26.8			c0-58.3,34.2-111.8,87.1-136.4c18.1-8.6,37.5-13.4,57.6-14.3c5.5-0.2,9.7-4.9,9.5-10.3c-0.2-5.5-4.8-9.7-10.3-9.5			c-22.8,1-44.7,6.4-65.2,16.1c-59.8,27.9-98.4,88.5-98.4,154.4v24.2l-31.6,60.3c-5.6,10.7-5.9,21.6-0.8,30s14.9,13.2,26.9,13.2h5.5			v49.3c0,24.1,19.6,43.6,43.6,43.6c0.5,0,1,0,1.5-0.1l34.9-5.2v35.2c0,5.5,4.4,9.9,9.9,9.9h189.7c5.5,0,9.9-4.4,9.9-9.9v-128.2			c16-15.4,28.6-33.5,37.6-53.8C365.323,263.815,363.023,258.015,358.023,255.815z"/><path style="fill:#2C2F33;" d="M450.723,29.915h-182.4c-22,0-39.9,17.9-39.9,39.9v111.3c0,22,17.9,39.9,39.9,39.9h13.6v43.2			c0,4,2.4,7.5,6,9.1c1.3,0.5,2.6,0.8,3.9,0.8c2.5,0,5-0.9,6.9-2.8l52.6-50.4h99.3c22,0,39.9-17.9,39.9-39.9v-111.2			C490.623,47.815,472.723,29.915,450.723,29.915z M470.823,181.115c0,11.1-9,20.1-20.1,20.1h-103.3c-2.6,0-5,1-6.8,2.8l-38.7,37.1			v-30c0-5.5-4.4-9.9-9.9-9.9h-23.7c-11.1,0-20.1-9-20.1-20.1v-111.3c0-11.1,9-20.1,20.1-20.1h182.3c11.1,0,20.1,9,20.1,20.1v111.3			L470.823,181.115L470.823,181.115z"/><circle style="fill:#3C92CA;" cx="359.523" cy="125.315" r="12.8"/><circle style="fill:#3C92CA;" cx="404.323" cy="125.315" r="12.8"/><circle style="fill:#3C92CA;" cx="314.823" cy="125.315" r="12.8"/></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>'/>
                    </View>

                    <View style={{flexDirection: 'column', justifyContent: 'center', backgroundColor: 'white'}}>
                        <Text style={{textAlign: 'center'}}>{this.props.navigation.state.params.role ? 'SIGNUP AS A BUSINESS/SERVICE' : <Text style={{fontSize: 13,fontWeight:'bold',color:'#8a8f96'}}>QUICK LOGIN OR SIGNUP</Text>}</Text>
                        <View style={{ height: 20, marginTop: 10, marginBottom: 15, width: width}}><Text></Text></View>
                        <Text style={{marginLeft: 15,fontWeight:'bold'}}>NAME</Text>
                        <TextInput
                            style={styles.input}
                            textInputProps={{ underlineColorAndroid: 'white'}}
                            onBlur={value => this.setState({nameError: name.length < 2})}
                            onChangeText={value => this.setState({ name: value })}
                            underlineColorAndroid="transparent"
                            placeholder={'E.g. John Doe'}
                            value={name}
                            />
                        {nameError && <Text style={{color: 'red', paddingLeft: 15}}>Name must be atleast 2 characters</Text>}
                        <View style={{ height: 15, width: width}}><Text></Text></View>
                        <Text style={{marginLeft: 15,fontWeight:'bold'}}>PHONE NUMBER</Text>
                        <PhoneInput ref='phone' style={styles.input} textStyle={{fontSize: 20}} textProps={{maxLength : 13}} initialCountry={'in'} onChangePhoneNumber={value => this.setState({ phoneNumber: value })}/>
                        {numberError && <Text style={{color: 'red', paddingLeft: 15}}>Number must be atleast 10 characters</Text>}
                        <Text style={{margin: 15,fontSize: 13,fontWeight:'bold',color:'#8a8f96'}}>Enter 10 digit phone number. OTP will be sent to this number for verification</Text>
                        {this.state.error && <View style={{margin: 15}}>
                            <Text style={{color: 'red', textAlign: 'center'}}>{this.state.message}</Text>
                        </View>}
                    </View>
                </ScrollView>


                {keyboardOpened || <View style={styles.footer}>
                    <TouchableOpacity onPress={this.signIn} style = {{flex: 1, flexDirection: 'row', width : 410}}>
                       {this.state.loading ? <View style = {{alignSelf: 'center', width : 20,flex: 1, alignItems:'center'}}>
                        <Image style={{height : 20, width : 20}}
                               source= {require('../../../assets/loader.gif')}
                            />
                        </View> :
                        <Text style={styles.footerText}>NEXT ></Text>}
                    </TouchableOpacity>
                </View>
                }
            </View>
        );
    }
    interval(){
        interval = setInterval(()=> {
            this.setState({timer: this.state.timer - 1});
            if(!this.state.timer){
                clearInterval(interval);
            }
        }, 1000);
    }
        renderMessage() {
        const { message } = this.state;
        if (!!message.length) return null;
        return (
            <Text style={{ padding: 5, backgroundColor: '#000', color: '#fff' }}>{message}</Text>
        );
    }
    renderVerificationCodeInput() {
        const { codeInput, keyboardOpened } = this.state;
        return (
            <View style={{flex: 1, flexDirection: 'column', justifyContent: 'space-between', alignItems: 'space-between', backgroundColor: 'white'}}>
                <ScrollView>
                    <View style={{alignItems: 'center',padding:10,marginTop:5}}>
                        <SvgUri width="70" height="70" svgXmlData='<?xml version="1.0" encoding="iso-8859-1"?> <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"viewBox="0 0 490.524 490.524" style="enable-background:new 0 0 490.524 490.524;" xml:space="preserve"><g><g><path style="fill:#2C2F33;" d="M358.023,255.815c-5-2.2-10.8,0-13.1,5.1c-8.5,19.1-20.6,36-36.1,50.2c-2,1.9-3.2,4.5-3.2,7.3			v122.6h-169.8v-36.8c0-2.9-1.3-5.6-3.4-7.5c-2.2-1.9-5.1-2.7-7.9-2.3l-45.5,6.8c-12.8-0.4-23.2-10.9-23.2-23.8v-59.3			c0-5.5-4.4-9.9-9.9-9.9h-15.4c-4.9,0-8.6-1.4-10-3.6c-1.4-2.3-0.9-6.2,1.4-10.5l32.7-62.4c0.7-1.4,1.1-3,1.1-4.6v-26.8			c0-58.3,34.2-111.8,87.1-136.4c18.1-8.6,37.5-13.4,57.6-14.3c5.5-0.2,9.7-4.9,9.5-10.3c-0.2-5.5-4.8-9.7-10.3-9.5			c-22.8,1-44.7,6.4-65.2,16.1c-59.8,27.9-98.4,88.5-98.4,154.4v24.2l-31.6,60.3c-5.6,10.7-5.9,21.6-0.8,30s14.9,13.2,26.9,13.2h5.5			v49.3c0,24.1,19.6,43.6,43.6,43.6c0.5,0,1,0,1.5-0.1l34.9-5.2v35.2c0,5.5,4.4,9.9,9.9,9.9h189.7c5.5,0,9.9-4.4,9.9-9.9v-128.2			c16-15.4,28.6-33.5,37.6-53.8C365.323,263.815,363.023,258.015,358.023,255.815z"/><path style="fill:#2C2F33;" d="M450.723,29.915h-182.4c-22,0-39.9,17.9-39.9,39.9v111.3c0,22,17.9,39.9,39.9,39.9h13.6v43.2			c0,4,2.4,7.5,6,9.1c1.3,0.5,2.6,0.8,3.9,0.8c2.5,0,5-0.9,6.9-2.8l52.6-50.4h99.3c22,0,39.9-17.9,39.9-39.9v-111.2			C490.623,47.815,472.723,29.915,450.723,29.915z M470.823,181.115c0,11.1-9,20.1-20.1,20.1h-103.3c-2.6,0-5,1-6.8,2.8l-38.7,37.1			v-30c0-5.5-4.4-9.9-9.9-9.9h-23.7c-11.1,0-20.1-9-20.1-20.1v-111.3c0-11.1,9-20.1,20.1-20.1h182.3c11.1,0,20.1,9,20.1,20.1v111.3			L470.823,181.115L470.823,181.115z"/><circle style="fill:#3C92CA;" cx="359.523" cy="125.315" r="12.8"/><circle style="fill:#3C92CA;" cx="404.323" cy="125.315" r="12.8"/><circle style="fill:#3C92CA;" cx="314.823" cy="125.315" r="12.8"/></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>'/>
                    </View>

                    <View style={{flexDirection: 'column', justifyContent: 'center', backgroundColor: 'white'}}>
                        <View>
                            <Text style={{textAlign: 'center'}}> {this.props.navigation.state.params.role ? 'SIGNUP AS A BUSINESS/SERVICE' : <Text style={{fontSize: 13,fontWeight:'bold',color:'#8a8f96'}}>QUICK LOGIN OR SIGNUP</Text>}</Text>
                        </View>
                        <View style={{ height: 20, marginTop: 15, marginBottom: 15, width: width}}><Text></Text></View>
                        <View style={{justifyContent: 'center', alignItems: 'center'}}>
                            <Text style={{fontWeight:'bold'}}>VERIFY OTP</Text>
                            <TextInput
                                autoFocus
                                style={{ height: 70, marginTop: 15, marginBottom: 15, width: width, fontSize: 20, textAlign: 'center', borderWidth: 1, borderColor: '#efefef'}}
                                caretHidden={false}
                                onChangeText={value => this.setState({ codeInput: value })}
                                placeholder={'XXXXXX '}
                                maxLength={6}
                                undelineColorAndroid="transparent"
                                keyboardType = "numeric"
                                value={codeInput}
                                />
                            <Text style={{textAlign: 'center',fontWeight:'bold',color:'#8a8f96' }}>Enter 6 digit OTP sent to phone number provided.</Text>
                        </View>
                        <View style={{marginTop: 15, justifyContent: 'center', alignItems: 'center'}}>
                            <Text style={{fontWeight:'bold',fontSize:20}}>0.<Text style={{fontWeight:'bold',fontSize:20}}>{this.state.timer}</Text></Text>
                        </View>
                        {!this.state.timer &&
                        <View style={{marginTop: 15, justifyContent: 'center', alignItems: 'center'}}>
                            <Text style={{textAlign: 'center',fontWeight:'bold'}}>Didn't receive OTP? Resend</Text>
                        </View>
                        }
                    </View>
                </ScrollView>
                {keyboardOpened || this.state.codeInput.length == 6 && <View style={styles.footer}>
                    <TouchableOpacity onPress={this.confirmCode} style = {{flex: 1, flexDirection: 'row', width : 410 }}>
                        {this.state.loading ? <View style={{alignSelf: 'center', flex: 1, alignItems:'center'}}>
                            <Image style={{height : 20, width : 20, marginTop : 16,  textAlign : 'center' }}
                               source= {require('../../../assets/loader.gif')}
                            /></View> :
                        <Text style={styles.footerText}>VERIFY ></Text>}
                    </TouchableOpacity>
                </View>
                }
            </View>
        );
    }

    navigateTo(){
        const {userBusiness} = this.props;
        this.props.navigation.navigate(userBusiness && Object.keys(userBusiness).length ? 'ChatsTab' : 'Profile');
    }

    render() {
        const { confirmResult, isConfirmed } = this.state;
        const { user } = this.props;
        return (
            <View style={{  flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', backgroundColor: 'white' }}>
                {!user && !confirmResult && this.renderPhoneNumberInput()}
                {!user && confirmResult && !isConfirmed && this.renderVerificationCodeInput()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    input: {
        height: 60,
        marginTop: 8,
        paddingLeft: 15,
        width: width,
        fontSize: 20,
        borderWidth: 1,
        backgroundColor: '#fafafa',
        borderColor: '#efefef'
    },
    footer: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "#4990d0",
        height: 50,
        width: width
    },
    footerText: {
        marginLeft : 165,
        marginTop : 8,
        color: "white",
        fontSize: 25,
        fontWeight: 'bold'
    }
});
const mapStateToProps = (state) => {
    return {
        user: state.userReducer.user,
        userBusiness: state.storeReducer.userBusiness
    };
};
const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        UPDATE_USER
    }, dispatch)
};
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login)
