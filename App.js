/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
//import Splash from './app/components/Splash/Splash';
import store from './app/redux/store.js';


import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TouchableOpacity,
    Switch,
    Dimensions
} from 'react-native';

import { Provider, connect } from 'react-redux';
import {DrawerNavigator, StackNavigator, NavigationActions, TabNavigator, DrawerItems} from 'react-navigation';
import Login from './app/components/auth/Login';
import ChatsTab from './app/components/chatsTab/ChatsTab';
import NearByTab from './app/components/nearByTab/NearByTab';
import Location from './app/components/nearByTab/Location';
import Profile from './app/components/profile/Profile';
import ChatsList from './app/components/chats/ChatsList';
import Info from './app/components/info/Info';
import Header from './app/components/header/Header';
import firebase from 'react-native-firebase';
import AddressBook from './app/components/addressBook/AddressBook'
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';

const {height,width} = Dimensions.get("window")

const stack2 = StackNavigator({
    ChatsTab:{
        screen: ChatsTab
    },
    Login:{
        screen: Login
    }
});



signOut = () => {
    firebase.auth().signOut();
};

console.log('app.js running');

// this shall be called regardless of app state: running, background or not running. Won't be called when app is killed by user in iOS



const Tab = TabNavigator({
        NearByTab: {
            screen: NearByTab,
            navigationOptions: () => ({
                title: 'NEARBY',
                drawerLockMode: 'locked-closed'
            })
        },
        ChatsTab: {
            screen: stack2,
            navigationOptions: ({navigation}) => ({
                title: 'CHATS',
                drawerLockMode: 'locked-closed'
            })
        }},
    {
        tabBarOptions: {
            style: {
                backgroundColor: 'white',
            },
            tabStyle: {
                height: 26
            },
            indicatorStyle: {
                borderBottomColor: 'black',
                borderBottomWidth: 2
            },
            activeTintColor: 'black',
            inactiveTintColor: '#c2c4c2'
        }
    }

);

const stack = StackNavigator({
    Home:{
        screen: Tab,
        navigationOptions: ({navigation}) => ({
            header: <Header navigate={navigation.navigate} data={{}}/>
        })
    },
    Info: {
        screen: Info,
        navigationOptions: ({navigation}) => ({
            header: <Header navigate={navigation.navigate} data={navigation.state.params.data} userToBusiness={navigation.state.params.userToBusiness} goBack={navigation.goBack}/>
        })
    },
    ChatsList: {
        screen: ChatsList,
        navigationOptions: ({navigation}) => ({
            header: <Header navigate={navigation.navigate} data={{}} business={true} userToBusiness={false} goBack={navigation.goBack}/>
        })
    },
    AddressBook: {
        screen: AddressBook,
        navigationOptions: ({navigation}) => ({
            header: <Header navigate={navigation.navigate} data={{}}  goBack={navigation.goBack}/>
        })
    }
});
//-----------------------------------------------
const AB = StackNavigator({
    stack: {
      screen: stack
    },
  });
const navigateOnce = (getStateForAction) => (action, state) => {
    const {type, routeName} = action;
    return (
      state &&
      type === NavigationActions.NAVIGATE &&
      routeName === state.routes[state.routes.length - 1].routeName
    ) ? null : getStateForAction(action, state);
    // you might want to replace 'null' with 'state' if you're using redux (see comments below)
  };
  
  AB.router.getStateForAction = navigateOnce(AB.router.getStateForAction);
  stack.router.getStateForAction = navigateOnce(stack.router.getStateForAction);
//-----------------------------------------
const profile = StackNavigator({
    Homee:{
        screen: Profile,
        navigationOptions: ({navigation}) => {
            return({
            header: <Header navigate={navigation.navigate} data={{}} subtitle={'Setup Business Profile'}  goBack={() => navigation.navigate("Home")}/>
        })}
    }
});

const mapStateToProps=(state)=>({
    name: state.userReducer.user ? state.userReducer.user.displayName : "Getting Your Name",
    phone: state.userReducer.user ? state.userReducer.user.phoneNumber : "Getting Your Number",
    userId:state.userReducer.user ? state.userReducer.user.uid : "fething...",
    userPresence : state.userReducer.userPressence || {},
    userBusiness: state.storeReducer.userBusiness,
    businessActivated: state.storeReducer.businessActivated
});

class DrawerDisplay extends Component{
    constructor(props){
        super(props);
    }
    render(){
        const {name , userId , userPresence , phone, userBusiness, businessActivated} = this.props;
        return(
            <View style={{flex: 1, flexDirection: 'column', justifyContent: 'space-between'}}>
               <View style = {{flex:1, marginTop :20 ,justifyContent: 'center', alignItems: 'center'}}>
                   <Image
                       source={require('./assets/user.png')}
                       style={{
                                width: 50,
                                height: 50,
                                borderRadius: 50,
                                borderWidth: 2,
                                borderColor: 'white'
                            }} />
                   <View style={{flex: 1, flexDirection: 'row'}}>
                       {userPresence[userId] ?
                           <Image source={require("./assets/online.png")}
                                  style={{
                                            marginTop: 5,
                                            width: 15,
                                            height: 15,
                                            borderRadius: 50,
                                            borderWidth: 2,
                                            borderColor: 'white'
                                        }}
                           /> :
                           <Image source={require("./assets/offline.png")}
                                  style={{
                                        width: 15,
                                        height: 15,
                                        borderRadius: 50,
                                        borderWidth: 2,
                                        borderColor: 'white'
                                    }}
                           />
                       }
                       <Text style={{ fontSize: 18, fontWeight: 'bold', textAlign: 'center' }}>{businessActivated ? userBusiness.businessName : name}</Text>
                   </View>
                   <View style={{flex: 2}}><Text>{phone}</Text></View>
               </View>
               <View></View>
               <View style = {{height : 300}}>
               <DrawerItems {...this.props} />
                   <View style={{backgroundColor: 'white', position: 'absolute', width: '100%'}}>
                       {userBusiness && userBusiness.businessName ?
                           <TouchableOpacity onPress={() => this.props.navigation.navigate(businessActivated ? 'Home' : 'ChatsList')}>

                                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', marginLeft: 15, marginBottom: 20, borderBottomWidth: 1, borderBottomColor: '#efefef', borderTopWidth: 1, borderTopColor: '#efefef', paddingTop: 20, paddingBottom: 8 }}>
                                    
                                    <Text style={{ fontSize: 16, color: '#032456', fontWeight: 'bold', width: '70%' }} numberOfLines={1}>{businessActivated ? name : userBusiness.businessName}</Text>
                                    <Switch />
                                </View>

                           </TouchableOpacity>
                           :
                           <TouchableOpacity onPress={() => this.props.navigation.navigate('Profile')}>
                               <Text style={{ fontSize: 16, marginLeft: 15, marginBottom: 20, borderBottomWidth: 1, borderBottomColor: '#efefef', borderTopWidth: 1, borderTopColor: '#efefef', color: '#032456', paddingTop: 20, paddingBottom: 8}}>
                                   Add Business Profile
                               </Text>
                           </TouchableOpacity>
                       }
                       <TouchableOpacity onPress={()=>{this.props.navigation.navigate("AddressBook")}}>
                           <Text style={{fontSize: 16, marginLeft: 15, marginBottom: 20, borderBottomWidth: 1, borderBottomColor: '#efefef', color: '#032456', paddingBottom: 8}}>
                               Address Book
                           </Text>
                       </TouchableOpacity>
                       <TouchableOpacity>
                           <Text style={{fontSize: 16, marginLeft: 15, marginBottom: 20, borderBottomWidth: 1, borderBottomColor: '#efefef', color: '#032456', paddingBottom: 8}}>
                               Profile
                           </Text>
                       </TouchableOpacity>
                       <TouchableOpacity>
                           <Text style={{fontSize: 16, marginLeft: 15, marginBottom: 20, borderBottomWidth: 1, borderBottomColor: '#efefef', color: '#032456', paddingBottom: 8}}>
                               Preference
                           </Text>
                       </TouchableOpacity>
                       <TouchableOpacity onPress={() => signOut()}>
                           <Text style={{fontSize: 16, marginLeft: 15, marginBottom: 10, borderBottomWidth: 1, borderBottomColor: '#efefef', color: '#032456', paddingBottom: 8}}>
                               Sign out
                           </Text>
                       </TouchableOpacity>
                   </View>
               </View>
               <View>
                   <TouchableOpacity>
                       <Text style={{ fontSize: 16, marginLeft: 15, marginBottom: 20, borderBottomWidth: 1, borderBottomColor: '#efefef', borderTopWidth: 1, borderTopColor: '#efefef', color: '#032456', paddingTop: 20, paddingBottom: 8}}>
                           Help & Tutorials
                       </Text>
                   </TouchableOpacity>
                   <TouchableOpacity>
                       <Text style={{fontSize: 16, marginLeft: 15, borderBottomWidth: 1, borderBottomColor: '#efefef', color: '#032456', paddingBottom: 8}}>
                           Send Feedback
                       </Text>
                   </TouchableOpacity>
               </View>
       </View>
        )
    }
}
const ContentComponent=connect(mapStateToProps)(DrawerDisplay)
const DrawerNavigatorConfig =  { //slider style of menu
    drawerWidth: 190,
    drawerPosition: 'right',
    contentComponent :ContentComponent
        // props =>
,

    contentOptions: {

        activeBackgroundColor: '#032456',
        activeTintColor: 'white',
        inactiveTintColor: '#032456',
        inactiveBackgroundColor: 'transparent'
    }
};

const Drawer = DrawerNavigator({
    Location: {
        screen: Location,
        header: null
    },
    Home: {
        screen: stack,
        navigationOptions: {
            header: null,
            drawer: () => ({
                label: 'Chat',
                icon: ({ tintColor }) => <Icon name="rocket" size={24} />
            })
        }

    },
    Profile: {
        screen: profile,
        navigationOptions: {
            title: 'Business Profile',
            drawer: () => ({
                label: 'Business Profile',
                icon: ({ tintColor }) => <Icon name="rocket" size={24} />
            })
        }
    }
}, DrawerNavigatorConfig);

const SplashNav = StackNavigator({
        Drawer: {
            screen: Drawer,
            navigationOptions: {
                drawerLockMode: 'locked-closed'
            }
        }
    },
    {
        headerMode: 'none'
    }
);

// gets the current screen from navigation state
function getCurrentRouteName(navigationState) {
    if (!navigationState) {
        return null;
    }
    const route = navigationState.routes[navigationState.index];
    // dive into nested navigators
    if (route.routes) {
        return getCurrentRouteName(route);
    }
    return route.routeName;
}


export default class App extends Component<{}> {
    constructor(){
        super();
    }

    render() {
        return (
            <Provider store={store}>
                <SplashNav
                    onNavigationStateChange={(prevState, currentState) => {
                    const currentScreen = getCurrentRouteName(currentState);
                    const prevScreen = getCurrentRouteName(prevState);
              }}
                />
            </Provider>
        );
    }
}
